package com.dadovicn.repo.primary.fx;

import com.dadovicn.constant.SysDict;
import com.dadovicn.fx.ywb.YwbBjYjxxb;
import com.dadovicn.fx.ywb.dto.TreeSelectDto;
import com.dadovicn.fx.ywb.vo.*;
import com.dadovicn.web.Result;

import java.util.List;
import java.util.Map;

/**
 * 获取所有案件类型
 * @author dadovicn
 * @date   2018/9/27
 */
public interface CommonRepo {
    /**
     * 获取所有报警类别
     * @return 报警类型集合
     */
    List<String> findAllBjlb();

    /**
     * 获取所有报警类别, 数量, map
     * @return
     */
    Map<String, Object> findAllBjlbMap();

    /**
     * 查找所有辖区名称
     * @return 辖区名称列表
     */
    List<String> findAllXQMC();

    /**
     * 查找所有报警类别代码和名称 | 报警类别表
     * @return list
     */
    List<TreeSelectDto> listAllBjlb();

    /**
     * 查找所有报警类型 | 报警类型表
     * @param lbdm 报警类别代码
     * @return
     */
    List<TreeSelectDto> findLxByLbdm(String lbdm);

    /**
     * 查找所有报警细类 | 报警细类
     * @param lxdm 报警类型代码
     * @return
     */
    List<TreeSelectDto> findXlByLxdm(String lxdm);

    /**
     * 查找所有报警辖区代码和名称
     * @return list
     */
    List<XqVO> listAllXq();

    /**
     * 根据父类字典键查找值
     * @return list
     */
    Result findDictByParentId(Long parentId, int currentPage, int pageSize);

    /**
     * 查询预警信息列表
     * @return
     */
    Result listYjxx();

    /**
     * 获取预警设置
     * @return
     */
    Result getYjxx();

    /**
     * 保存预警设置
     * @param ywbBjYjxxb
     * @return
     */
    Result updateYjxx(YwbBjYjxxb ywbBjYjxxb);

}