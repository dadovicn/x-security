package com.dadovicn.repo.primary.fx;

import com.dadovicn.fx.ywb.dto.PieDTO;
import com.dadovicn.fx.ywb.vo.ReportVO;
import com.dadovicn.web.Result;
import com.querydsl.core.BooleanBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 警情分析
 * @author dadovicn
 * @date   2018/10/2
 */
public interface JjAnalysisRepo {
    /**
     * 获取 [年-月] 报表数据
     * @return
     */
    Result getReportYearMonth();

    /**
     * 获取 [月-天] 报表数据
     * @return
     */
    Result getReportMonthDay();

    /**
     * 获取单月报表数据
     * @return
     */
    ReportVO getReportByTime(BooleanBuilder builder, String xqdm);

    /**
     * 获取所有报警类别: 数量
     * @return
     */
    Result getBjlbPie(List<LocalDateTime> date, String pieXqdm);

    /**
     * 报警类型玫瑰图
     * @return
     */
    Result getBjlxPolarintervalByBjlbmc(String bjlbmc, List<LocalDateTime> date, String pieXqdm);

    Result getBjxlPyramidByBjlxmc(String bjlxmc, List<LocalDateTime> date, String pieXqdm);

}
