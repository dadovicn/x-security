package com.dadovicn.repo.primary.fx;


import com.dadovicn.fx.ywb.YwbJqJjjlb;
import com.dadovicn.fx.ywb.dto.YwbJjjlbCountADto;
import com.dadovicn.fx.ywb.qo.CalendarQO;
import com.dadovicn.fx.ywb.qo.CountQO;
import com.dadovicn.fx.ywb.qo.JcjQO;
import com.dadovicn.repo.primary.base.BaseQueryDsl;
import com.dadovicn.web.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.servlet.http.HttpServletResponse;

/**
 * 接处警基本查询接口
 * @author dadovicn
 * @date   2018/9/11
 */
public interface JcjRepo {
    /**
     * 根据接警编号查询详情
     * @param jjbh
     * @return
     */
    Result findJcjByJjbh(String jjbh);


    /**
     * 根据接警编号查出警单位
     * @param jjbh
     * @return
     */
    Result findCjjByJjbh(String jjbh);
    /**
     * 获取接处警数据查询结果
     * @param q 查询对象
     * @param currentPage 分页
     * @param pageSize 页码
     * @return result
     */
    Result listJcj(JcjQO q, int currentPage, int pageSize);

    /**
     * 导出查询结果集
     * @param q
     * @param response
     */
    void jcjExport(JcjQO q, HttpServletResponse response);


    /**
     * 当日接警总量, 环比
     * @return result
     */
    Result countYwbJqJjjlbToday();

    /**
     * 当日出警总量, 环比
     * @return result
     */
    Result countYwbJqCjjlbToday();

    /**
     * 当日有效和无效总量, 环比
     */
    Result countYwbJqJjjlbValid();


    /**
     * 获取当日刑事案件总量
     * @return
     */
    Result countXsajToday();

    /**
     * 词云: 当日高频案件类型
     * @return result
     */
    Result listHighFreq(CountQO qo);

    /**
     * 主要监控台
     * @return result
     */
    Result primaryMonitor(CountQO qo);

    /**
     * 主要监控台 出警数据
     * @return result
     */
    Result primaryMonitorForCj(CountQO qo);

    /**
     * 辖区排名
     * @return result
     */
    Result xqRanking(CountQO qo);

    /**
     * 辖区出警排名
     * @return result
     */
    Result xqRankingCj(CountQO qo);

    /**
     * 获取近一年水平日历热力图
     * @return result
     */
    Result getCalendarhorizontalData(CalendarQO calendarQO);

    /**
     * 预警图
     * @return
     */
    Result getHeatMap();

    /**
     * 获取近一月趋势数据
     * @return
     */
    Result getRecentMonthJj();

    /** 日增量*/
    Result getDayRate();

    /**
     * 周增量
     * @return
     */
    Result getWeekRate();

    /**
     * 月增量
     * @return
     */
    Result getMonthRate();

    /**
     * 获取月预警
     * @return
     */
    Result getWarnMonth();

    /**
     * 获取月预警
     * @return
     */
    Result getWarnWeek();

    /**
     * 获取日预警
     * @return
     */
    Result getWarnDay();

}