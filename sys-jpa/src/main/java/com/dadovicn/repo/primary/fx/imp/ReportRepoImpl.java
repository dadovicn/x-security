package com.dadovicn.repo.primary.fx.imp;

import com.dadovicn.repo.primary.base.BaseQueryDsl;
import com.dadovicn.repo.primary.fx.ReportRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 报表数据接口
 * @author dadovicn
 * @date   2018/10/3
 */
@Transactional
@Slf4j
@Service
public class ReportRepoImpl extends BaseQueryDsl implements ReportRepo {

}