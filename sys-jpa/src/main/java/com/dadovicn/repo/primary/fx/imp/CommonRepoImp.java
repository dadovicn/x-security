package com.dadovicn.repo.primary.fx.imp;

import com.dadovicn.constant.SysDict;
import com.dadovicn.constant.dsl.QSysDict;
import com.dadovicn.fx.jcb.dsl.QJcbBjBjlbb;
import com.dadovicn.fx.jcb.dsl.QJcbBjBjlxb;
import com.dadovicn.fx.jcb.dsl.QJcbBjBjxlb;
import com.dadovicn.fx.jcb.dsl.QJcbBjXqb;
import com.dadovicn.fx.ywb.YwbBjYjxxb;
import com.dadovicn.fx.ywb.dsl.QYwbBjYjxxb;
import com.dadovicn.fx.ywb.dsl.QYwbJqCjjlb;
import com.dadovicn.fx.ywb.dto.TreeSelectDto;
import com.dadovicn.fx.ywb.dto.YjxxDTO;
import com.dadovicn.fx.ywb.vo.*;
import com.dadovicn.repo.primary.base.BaseQueryDsl;
import com.dadovicn.repo.primary.fx.CommonRepo;
import com.dadovicn.web.Result;
import com.fasterxml.jackson.databind.util.BeanUtil;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
@Transactional
public class CommonRepoImp extends BaseQueryDsl implements CommonRepo {
    /// todo 0002 需要处理查询中的异常情况
    @Override
    public List<String> findAllBjlb() {
        QJcbBjBjlbb q = QJcbBjBjlbb.jcbBjBjlbb;
        JPAQuery jpaQuery = queryFactory.select(  q.bjlbmc)
                .from(q)
                .groupBy(q.bjlbmc)
                .fetchAll();
        List<String> result = jpaQuery.fetch();
        return result;
    }

    public Map<String, Object> findAllBjlbMap() {
        QJcbBjBjlbb q = QJcbBjBjlbb.jcbBjBjlbb;
        Map<String, Object> result = new HashMap<>();
        queryFactory.select(  q.bjlbmc, q.bjlbmc.count())
                .from(q)
                .groupBy(q.bjlbmc).fetch().stream().forEach(tuple -> {
                    result.put((String)tuple.get(q.bjlbmc), 0L);
                });
        return result;
    }


    @Override
    public List<String> findAllXQMC() {
        QJcbBjXqb q = QJcbBjXqb.jcbBjXqb;
        JPAQuery jpaQuery = queryFactory.select(q.xqmc)
                .from(q)
                .groupBy(q.xqmc)
                .fetchAll();
        List<String> result = jpaQuery.fetch();
        return result;
    }

    @Override
    public List<TreeSelectDto> listAllBjlb() {
        QJcbBjBjlbb q = QJcbBjBjlbb.jcbBjBjlbb;
        JPAQuery jpaQuery = queryFactory.select(Projections.bean(BjlbVO.class, q.bjlbmc, q.bjlbdm))
                .from(q)
                .fetchAll();
        List<BjlbVO> bjlbVOList = jpaQuery.fetch();
        List<TreeSelectDto> result = bjlbVOList.stream().map(i -> {
            return new TreeSelectDto(i.getBjlbdm(), i.getBjlbmc(), false);
        }).collect(Collectors.toList());
        return result;
    }

    @Override
    public List<TreeSelectDto> findLxByLbdm(String lbdm) {
        QJcbBjBjlxb q = QJcbBjBjlxb.jcbBjBjlxb;
        JPAQuery jpaQuery = queryFactory.select(Projections.bean(BjlxVO.class, q.bjlxmc, q.bjlxdm))
                .from(q)
                .where(q.bjlbdm.eq(lbdm))
                .fetchAll();
        List<BjlxVO> bjlxVOList = jpaQuery.fetch();
        List<TreeSelectDto> result = bjlxVOList.stream().map(i -> {
            return new TreeSelectDto(i.getBjlxdm(), i.getBjlxmc(), false);
        }).collect(Collectors.toList());
        return result;
    }

    @Override
    public List<TreeSelectDto> findXlByLxdm(String lxdm) {
        QJcbBjBjxlb q = QJcbBjBjxlb.jcbBjBjxlb;
        JPAQuery jpaQuery = queryFactory.select(Projections.bean(BjxlVO.class, q.bjxlmc, q.bjxldm))
                .from(q)
                .where(q.bjlxdm.eq(lxdm))
                .fetchAll();
        List<BjxlVO> bjxlVOList = jpaQuery.fetch();
        List<TreeSelectDto> result = bjxlVOList.stream().map(i -> {
            return new TreeSelectDto(i.getBjxldm(), i.getBjxlmc(), true);
        }).collect(Collectors.toList());
        return result;

    }

    @Override
    public List<XqVO> listAllXq() {
        QJcbBjXqb q = QJcbBjXqb.jcbBjXqb;
        JPAQuery jpaQuery = queryFactory.select(Projections.bean(XqVO.class, q.xqdm, q.xqmc))
                .from(q).fetchAll();
        List<XqVO> result = jpaQuery.fetch();
        return result;
    }

    @Override
    public Result findDictByParentId(Long parentId, int currentPage, int pageSize) {
        QSysDict qSysDict = QSysDict.sysDict;
        JPAQuery<SysDict> jpaQuery = queryFactory.select(qSysDict)
                .from(qSysDict)
                .where(qSysDict.parentId.eq(parentId))
                .fetchAll();
        if(pageSize == 0) {
            return Result.success(jpaQuery.fetch());
        }
        jpaQuery = jpaQuery.offset(currentPage * pageSize).limit(pageSize);
        Long a = jpaQuery.fetchCount();
        List<SysDict> re = jpaQuery.fetch();
        return Result.success(jpaQuery.fetch(), jpaQuery.fetchCount());
    }

    @Override
    public Result listYjxx() {
        QYwbBjYjxxb q = QYwbBjYjxxb.ywbBjYjxxb;
        JPAQuery jpaQuery  = queryFactory.select(q).from(q);
        return Result.success(jpaQuery.fetch());
    }

    public Result getYjxx() {
        QYwbBjYjxxb q = QYwbBjYjxxb.ywbBjYjxxb;
        JPAQuery jpaQuery = queryFactory.select(q).from(q);
        List<YwbBjYjxxb> ywbBjYjxxb = jpaQuery.fetch();
        List<Map<String, Object>> result = ywbBjYjxxb.stream().map(i -> {
            try {
                return objectToMap(i);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return null;
        }).collect(Collectors.toList());
        return Result.success(result);
    }

    @Override
    public Result updateYjxx(YwbBjYjxxb ywbBjYjxxb) {
        QYwbBjYjxxb q = QYwbBjYjxxb.ywbBjYjxxb;

        Long result = queryFactory.update(q)
                //更新字段列表
                .set(q.hzsg, ywbBjYjxxb.getHzsg())
                .set(q.jbts, ywbBjYjxxb.getJbts())
                .set(q.jtljq, ywbBjYjxxb.getJtljq())
                .set(q.qtjq, ywbBjYjxxb.getQtjq())
                .set(q.qtxsj, ywbBjYjxxb.getQtxsj())
                .set(q.qzqz, ywbBjYjxxb.getQzqz())
                .set(q.shld, ywbBjYjxxb.getShld())
                .set(q.xsaj, ywbBjYjxxb.getXsaj())
                .set(q.xzwf, ywbBjYjxxb.getXzwf())
                .set(q.zaaj, ywbBjYjxxb.getZaaj())
                .set(q.zhsg, ywbBjYjxxb.getZhsg())
                //更新条件
                .where(q.id.eq(ywbBjYjxxb.getId()))
                //执行更新
                .execute();
        return  getYjxx();
    }

    /**
     * 预警信息对象转 map
     * @return
     */
    public static Map<String, Object> objectToMap(Object obj) throws IllegalAccessException {
        if(obj == null){
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        Field[] declaredFields = obj.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            String filedName = field.getName();
            switch (filedName) {
                case "xsaj": map.put("刑事案件", field.get(obj)); break;
                case "zaaj": map.put("治安案件", field.get(obj)); break;
                case "qzqz": map.put("群众求助", field.get(obj)); break;
                case "jtljq": map.put("交通类警情", field.get(obj)); break;
                case "hzsg": map.put("火灾事故", field.get(obj)); break;
                case "jbts": map.put("举报投诉", field.get(obj)); break;
                case "shld": map.put("社会联动", field.get(obj)); break;
                case "qtxsj": map.put("群体性事件", field.get(obj)); break;
                case "zhsg": map.put("灾害事故", field.get(obj)); break;
                case "xzwf": map.put("行政违法", field.get(obj)); break;
                case "qtjq": map.put("其他警情", field.get(obj)); break;
                case "type": map.put("type", field.get(obj)); break;
                default: break;
            }
        }

        return map;
    }
}