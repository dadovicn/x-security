package com.dadovicn.repo.primary.auth;

import com.dadovicn.auth.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * create by 段董董 on 2018/8/15 - 下午5:59
 */
@Repository
public interface SysUserRepo extends PagingAndSortingRepository<SysUser, Long>, QuerydslPredicateExecutor<SysUser> {

    /**
     * 根据用户名查找当前用户
     * @param userName 用户名
     * @return SysUser
     */
    @Query("select user from SysUser user where user.userName = ?1")
    SysUser findSysUserByUserName(String userName);


}