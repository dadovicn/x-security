package com.dadovicn.repo.primary.fx.imp;

import com.dadovicn.repo.primary.base.BaseQueryDsl;
import com.dadovicn.repo.primary.fx.CjAnalysisRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 处警数据分析
 * @author dadovicn
 * @date   2018/10/4
 */
@Slf4j
@Component
@Transactional
public class CjAnalysisRepoImpl extends BaseQueryDsl implements CjAnalysisRepo {

}