package com.dadovicn.repo.primary.fx.imp;

import com.dadovicn.repo.primary.fx.CacheManageService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CacheManageServiceImpl implements CacheManageService {

    /**
     * 清缓存
     */
    @CacheEvict(value = "reportCache", allEntries = true)
    public void evictAllCaches() {
    }
}