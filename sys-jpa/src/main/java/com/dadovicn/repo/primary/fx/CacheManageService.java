package com.dadovicn.repo.primary.fx;

/**
 * create by 段董董 on 2018/11/18 - 下午11:03
 */
public interface CacheManageService {
    /**
     * 清所有缓存
     */
    void evictAllCaches();

}
