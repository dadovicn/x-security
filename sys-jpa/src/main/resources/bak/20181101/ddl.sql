-- 所有序列
create sequence SEQ_SYS_ROLE
maxvalue 999999999 ;

create sequence SEQ_SYS_LOCAL_AUTH
maxvalue 999999999 ;

create sequence SEQ_SYS_USER
maxvalue 999999999 ;

create sequence SEQ_USER_ROLE
maxvalue 999999999 ;

create SEQUENCE SEQ_YWB_BJ_YJXXB
maxvalue 999999999 ;

-- 用户表
create table SYS_USER (
  USER_ID NUMBER not null primary key,
  USER_NAME VARCHAR2(128),
  MOBILE VARCHAR2(11),
  EMAIL VARCHAR2(64),
  REAL_NAME VARCHAR2(128),
  GROUP_ID NUMBER,
  CREATE_TIME TIMESTAMP(6) not null,
  UPDATE_TIME TIMESTAMP(6),
  STATUS NUMBER(1) default 0 not null,
  LOCAL_AUTH_ID NUMBER,
  AVATAR_URL VARCHAR2(256),
  GENDER CHAR
) ;
comment on column SYS_USER.GENDER is 'm/f' ;

-- 用户-密码表
create table SYS_USER_LOCAL_AUTH
(
  ID NUMBER not null
    primary key,
  PASSWORD VARCHAR2(256),
  SALT VARCHAR2(16),
  CREATE_TIME TIMESTAMP(6),
  UPDATE_TIME TIMESTAMP(6),
  STATUS NUMBER(1)
) ;
comment on table SYS_USER_LOCAL_AUTH is '本地密码认证' ;
comment on column SYS_USER_LOCAL_AUTH.PASSWORD is '密码' ;
comment on column SYS_USER_LOCAL_AUTH.SALT is '盐' ;
comment on column SYS_USER_LOCAL_AUTH.STATUS is '0: false; 1: true;' ;

-- 角色表
create table SYS_ROLE(
  ROLE_ID NUMBER not null
    primary key,
  ROLE_NAME VARCHAR2(128),
  ROLE_DESC VARCHAR2(128)
) ;

-- 用户-角色表
create table SYS_USER_ROLE(
  ID NUMBER not null
    primary key,
  ROLE_ID NUMBER,
  USER_ID NUMBER
) ;
-- 预警信息表
create table YWB_BJ_YJXXB
(
  ID NUMBER not null
    primary key,
  XSAJ NUMBER,
  ZAAJ NUMBER,
  QZQZ NUMBER,
  JTLJQ NUMBER,
  HZSG NUMBER,
  JBTS NUMBER,
  SHLD NUMBER,
  QTXSJ NUMBER,
  ZHSG NUMBER,
  XZWF NUMBER,
  QTJQ NUMBER,
  TYPE VARCHAR2(64)
) ;

comment on table YWB_BJ_YJXXB is '预警信息表' ;




