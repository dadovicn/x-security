create sequence SEQ_SYS_DICT maxvalue 999999999;

create table SYS_DICT
(
  DICT_ID NUMBER not null primary key,
  PARENT_ID NUMBER not null,
  DICT_KEY VARCHAR2(128),
  DICT_VALUE VARCHAR2(128),
  DICT_DESC VARCHAR2(256),
  PATH VARCHAR2(128),
  NODE_TYPE VARCHAR2(36),
  CREATE_TIME DATE,
  UPDATE_TIME DATE,
  STATUS CHAR default 'Y',
  EDITABLE CHAR DEFAULT 'Y'
);

comment on table SYS_DICT is '字典表';
comment on column SYS_DICT.PARENT_ID is '父级id';
comment on column SYS_DICT.DICT_KEY is '字典键-这个应该是个英文, 这个值应该开发定的, 所以字典表只需提供修改字典值的接口即可(非得提供也是给开放给开发的)';
comment on column SYS_DICT.DICT_VALUE is '字典值';
comment on column SYS_DICT.DICT_DESC is '描述-给用户看的, 用户依据这个修改字典值';
comment on column SYS_DICT.PATH is '路径枚举';
comment on column SYS_DICT.NODE_TYPE is '节点类型-';
comment on column SYS_DICT.CREATE_TIME is '创建时间';
comment on column SYS_DICT.UPDATE_TIME is '修改时间';
comment on column SYS_DICT.STATUS is 'Y 或 N 是否有效';
comment on column SYS_DICT.EDITABLE is 'Y 或 N 是否可编辑';

