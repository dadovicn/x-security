package com.dadovicn.util;

import com.dadovicn.enums.DayOfWeekEnum;
import com.dadovicn.enums.MonthOfYearEnum;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * 常用日期 util
 * @author dadovicn
 * @date   2018/10/1
 */
public class DateUtil {

    /**
     * 获取过去一年的天
     * @return
     */
    public static List<String> genYearDate() {
        List<String> result = new ArrayList<>();
        LocalDate now = LocalDate.now();
        LocalDate lastYear = now.minusYears(1);
        result.add(now.toString());
        System.out.println(lastYear.toString());
        while(!now.isEqual(lastYear)) {
            now = now.minusDays(1);
            result.add(now.toString());
        }
        return result;
    }

    /**
     * 获取近一月的天数
     * @return
     */
    public static List<String> genMonthDate() {
        List<String> result = new ArrayList<>();
        LocalDate now = LocalDate.now();
        LocalDate lastMonth = now.minusMonths(1);
        result.add(now.toString());
        while(!now.isEqual(lastMonth)) {
            now = now.minusDays(1);
            result.add(now.toString());
        }
        return result;
    }

    /**
     * 获取一周的第几天
     * @param localDate
     * @return
     */
    public static Integer getDayOfWeek(String localDate) {
        Integer result = Enum.valueOf(DayOfWeekEnum.class, LocalDate.parse(localDate, DateTimeFormatter.ISO_LOCAL_DATE).getDayOfWeek().toString()).getValue();
        return result;
    }

    /**
     * 获取月份
     * @param localDate
     * @return
     */
    public static Integer getMonth(String localDate) {
        Integer result = Enum.valueOf(MonthOfYearEnum.class, LocalDate.parse(localDate, DateTimeFormatter.ISO_LOCAL_DATE).getMonth().toString()).getValue();
        return result;
    }

    public static String getMonthAll(MonthOfYearEnum month) {

        Integer prefix = LocalDate.now().getYear();
        String result = prefix.toString();
        Integer suffix = month.getValue();
        if(suffix < 10) {
            result += '0' + suffix;
        }else {
            result += suffix;
        }
        return result;
    }
}