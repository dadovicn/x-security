package com.dadovicn.annoation;

import java.lang.annotation.*;

/**
 * 普通 and 字段标志
 * @author dadovicn
 * @date   2018/9/12
 */
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface And {
    boolean required() default true;
}
