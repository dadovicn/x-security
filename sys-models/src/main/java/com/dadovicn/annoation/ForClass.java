package com.dadovicn.annoation;

import com.querydsl.core.types.dsl.EntityPathBase;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ForClass {
    String name() default "";

    /**
     * 关联表键值对
     * @return
     */
    Class[] classPairs() default {} ;
}
