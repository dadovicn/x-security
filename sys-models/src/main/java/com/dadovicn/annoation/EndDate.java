package com.dadovicn.annoation;

import java.lang.annotation.*;

/**
 * 结束时间查询标志
 * @author dadovicn
 * @date   2018/9/12
 */
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EndDate {
    String value() default "";
}