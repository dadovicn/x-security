package com.dadovicn.annoation;

import java.lang.annotation.*;

/**
 * 开始时间查询标志
 * @author dadovicn
 * @date   2018/9/12
 */
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface StartDate {
    String value() default "";
}