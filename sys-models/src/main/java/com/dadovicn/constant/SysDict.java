package com.dadovicn.constant;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 系统字典表
 * @author dadovicn
 * @date   2018/10/2
 */
@Entity
@Data
@Table(name = "SYS_DICT", schema = "FX110", catalog = "")
public class SysDict {
    @Id
    @Column(name = "DICT_ID")
    private long dictId;
    @Basic
    @Column(name = "PARENT_ID")
    private Long parentId;
    @Basic
    @Column(name = "DICT_KEY")
    private String dictKey;
    @Basic
    @Column(name = "DICT_VALUE")
    private String dictValue;
    @Basic
    @Column(name = "DICT_DESC")
    private String dictDesc;
    @Basic
    @Column(name = "PATH")
    private String path;
    @Basic
    @Column(name = "NODE_TYPE")
    private String nodeType;
    @Basic
    @Column(name = "CREATE_TIME")
    private LocalDateTime createTime;
    @Basic
    @Column(name = "UPDATE_TIME")
    private LocalDateTime updateTime;
    @Basic
    @Column(name = "STATUS")
    private String status;
    @Basic
    @Column(name = "editable")
    private String editAble;
}