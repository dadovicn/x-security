package com.dadovicn.constant;

/**
 * 基础常量类
 * @author dadovicn
 * @date   2018/9/12
 */
public class Constant {
    /** 中国时间标准显示 */
    public static final String DATE_TODAY = "YYYY-MM-dd HH:mm:ss";
    public static final String DATE_FOR_DOWNLOAD = "YYYY-MM-dd HH-mm-ss";
}