package com.dadovicn.constant.dsl;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.dadovicn.constant.SysDict;
import com.querydsl.core.types.dsl.*;
import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSysDict is a Querydsl query type for SysDict
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSysDict extends EntityPathBase<SysDict> {

    private static final long serialVersionUID = -178566572L;

    public static final QSysDict sysDict = new QSysDict("sysDict");

    public final DateTimePath<java.time.LocalDateTime> createTime = createDateTime("createTime", java.time.LocalDateTime.class);

    public final NumberPath<Long> dictId = createNumber("dictId", Long.class);

    public final StringPath nodeType = createString("nodeType");

    public final StringPath dictDesc = createString("dictDesc");

    public final NumberPath<Long> parentId = createNumber("parentId", Long.class);

    public final StringPath path = createString("path");

    public final StringPath status = createString("status");

    public final StringPath editable = createString("editable");

    public final StringPath dictKey = createString("dictKey");

    public final StringPath dictValue = createString("dictValue");

    public final DateTimePath<java.time.LocalDateTime> updateTime = createDateTime("updateTime", java.time.LocalDateTime.class);

    public QSysDict(String variable) {
        super(SysDict.class, forVariable(variable));
    }

    public QSysDict(Path<? extends SysDict> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSysDict(PathMetadata metadata) {
        super(SysDict.class, metadata);
    }

}

