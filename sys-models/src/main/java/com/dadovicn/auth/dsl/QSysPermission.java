package com.dadovicn.auth.dsl;

import com.dadovicn.auth.SysPermission;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QSysPermission is a Querydsl query type for SysPermission
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSysPermission extends EntityPathBase<SysPermission> {

    private static final long serialVersionUID = -818962831L;

    public static final QSysPermission sysPermission = new QSysPermission("sysPermission");

    public final StringPath desc = createString("desc");

    public final StringPath name = createString("name");

    public final NumberPath<Long> permissionId = createNumber("permissionId", Long.class);

    public QSysPermission(String variable) {
        super(SysPermission.class, forVariable(variable));
    }

    public QSysPermission(Path<? extends SysPermission> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSysPermission(PathMetadata metadata) {
        super(SysPermission.class, metadata);
    }

}

