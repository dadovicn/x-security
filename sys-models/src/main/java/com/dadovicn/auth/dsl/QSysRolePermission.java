package com.dadovicn.auth.dsl;

import com.dadovicn.auth.SysRolePermission;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QSysRolePermission is a Querydsl query type for SysRolePermission
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSysRolePermission extends EntityPathBase<SysRolePermission> {

    private static final long serialVersionUID = 50347655L;

    public static final QSysRolePermission sysRolePermission = new QSysRolePermission("sysRolePermission");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> permissionId = createNumber("permissionId", Long.class);

    public final NumberPath<Long> roleId = createNumber("roleId", Long.class);

    public QSysRolePermission(String variable) {
        super(SysRolePermission.class, forVariable(variable));
    }

    public QSysRolePermission(Path<? extends SysRolePermission> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSysRolePermission(PathMetadata metadata) {
        super(SysRolePermission.class, metadata);
    }

}

