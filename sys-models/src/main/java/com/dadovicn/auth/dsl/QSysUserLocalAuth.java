package com.dadovicn.auth.dsl;

import com.dadovicn.auth.SysUserLocalAuth;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.*;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QSysUserLocalAuth is a Querydsl query type for SysUserLocalAuth
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSysUserLocalAuth extends EntityPathBase<SysUserLocalAuth> {

    private static final long serialVersionUID = -117270458L;

    public static final QSysUserLocalAuth sysUserLocalAuth = new QSysUserLocalAuth("sysUserLocalAuth");

    public final DateTimePath<java.sql.Timestamp> createTime = createDateTime("createTime", java.sql.Timestamp.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath password = createString("password");

    public final StringPath salt = createString("salt");

    public final BooleanPath status = createBoolean("status");

    public final DateTimePath<java.sql.Timestamp> updateTime = createDateTime("updateTime", java.sql.Timestamp.class);

    public QSysUserLocalAuth(String variable) {
        super(SysUserLocalAuth.class, forVariable(variable));
    }

    public QSysUserLocalAuth(Path<? extends SysUserLocalAuth> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSysUserLocalAuth(PathMetadata metadata) {
        super(SysUserLocalAuth.class, metadata);
    }

}

