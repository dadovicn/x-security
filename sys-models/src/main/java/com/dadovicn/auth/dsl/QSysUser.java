package com.dadovicn.auth.dsl;

import com.dadovicn.auth.SysRole;
import com.dadovicn.auth.SysUser;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.*;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QSysUser is a Querydsl query type for SysUser
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSysUser extends EntityPathBase<SysUser> {

    private static final long serialVersionUID = 1311794669L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSysUser sysUser = new QSysUser("sysUser");

    public final StringPath avatarUrl = createString("avatarUrl");

    public final DateTimePath<java.sql.Timestamp> createTime = createDateTime("createTime", java.sql.Timestamp.class);

    public final StringPath email = createString("email");

    public final ComparablePath<Character> gender = createComparable("gender", Character.class);

    public final NumberPath<Long> groupId = createNumber("groupId", Long.class);

    public final StringPath mobile = createString("mobile");

    public final StringPath realName = createString("realName");

    public final ListPath<SysRole, QSysRole> roles = this.<SysRole, QSysRole>createList("roles", SysRole.class, QSysRole.class, PathInits.DIRECT2);

    public final BooleanPath status = createBoolean("status");

    public final QSysUserLocalAuth sysUserLocalAuth;

    public final DateTimePath<java.sql.Timestamp> updateTime = createDateTime("updateTime", java.sql.Timestamp.class);

    public final NumberPath<Long> userId = createNumber("userId", Long.class);

    public final StringPath userName = createString("userName");

    public QSysUser(String variable) {
        this(SysUser.class, forVariable(variable), INITS);
    }

    public QSysUser(Path<? extends SysUser> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSysUser(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSysUser(PathMetadata metadata, PathInits inits) {
        this(SysUser.class, metadata, inits);
    }

    public QSysUser(Class<? extends SysUser> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sysUserLocalAuth = inits.isInitialized("sysUserLocalAuth") ? new QSysUserLocalAuth(forProperty("sysUserLocalAuth")) : null;
    }

}

