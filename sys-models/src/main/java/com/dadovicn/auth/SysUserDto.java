package com.dadovicn.auth;

import lombok.*;

/**
 * 用户 dto 对象
 * @author dadovicn
 * @date   2018/9/9
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUserDto {
    private long userId;
    private String userName;
    private String avatarUrl;
}