package com.dadovicn.auth;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * create by 段董董 on 2018/8/16 - 上午10:41
 */
@Entity
@Data
@ToString(exclude={"users"})
@Table(name = "SYS_ROLE")
public class SysRole {

    @Id@Column(name = "ROLE_ID")
    private long roleId;

    @Basic@Column(name = "ROLE_NAME")
    private String roleName;

    @Basic@Column(name = "ROLE_DESC")
    private String roleDesc;

}