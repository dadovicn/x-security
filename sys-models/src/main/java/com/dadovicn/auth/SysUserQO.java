package com.dadovicn.auth;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

/**
 * 用户查询 QO
 * @author dadovicn
 * @date   2018/9/3
 */
@Data
public class SysUserQO {
    private String userName;
    private String mobile;
    private String realName;
    private Long groupId;
    private Timestamp createTimeStart;
    private Timestamp createTimeEnd;
    private Timestamp updateTimeStart;
    private Timestamp updateTimeEnd;
    private boolean status;
}