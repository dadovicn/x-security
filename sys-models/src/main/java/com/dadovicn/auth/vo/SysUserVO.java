package com.dadovicn.auth.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 保存修改用户用
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SysUserVO {
    private Long userId;
    private String userName;
    private String realName;
    private String password;
}