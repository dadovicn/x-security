package com.dadovicn.enums;

/**
 * 获取月份数字
 * @author dadovicn
 * @date   2018/10/1
 */
public enum MonthOfYearEnum {

    JANUARY(1),
    FEBRUARY(2),
    MARCH(3),
    APRIL(4),
    MAY(5),
    JUNE(6),
    JULY(7),
    AUGUST(8),
    SEPTEMBER(9),
    OCTOBER(10),
    NOVEMBER(11),
    DECEMBER(12);

    private Integer value;

    MonthOfYearEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
