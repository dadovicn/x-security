package com.dadovicn.enums;

/**
 * 每周的第几天
 * @author dadovicn
 * @date   2018/10/2
 */
public enum DayOfWeekEnum {
    MONDAY(1),
    SUNDAY(0),
    SATURDAY(6),
    FRIDAY(5),
    THURSDAY(4),
    WEDNESDAY(3),
    TUESDAY(2);
    private Integer value;
    DayOfWeekEnum(Integer val) {
        this.value = val;
    }
    public Integer getValue() {
        return this.value;
    }
}
