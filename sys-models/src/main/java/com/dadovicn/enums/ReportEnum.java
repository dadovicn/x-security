package com.dadovicn.enums;

/**
 * 报表枚举
 * @author dadovicn
 * @date   2018/10/4
 */
public enum ReportEnum {
    /** 刑事案件-[盗窃] 01010000 */
    DAOQIE("01010000"),
    /** 刑事案件-[抢劫] 01020000 */
    QIANGJIE("01020000"),
    /** 刑事案件-[抢夺] 01030000 */
    QIANGDUO("01030000"),
    /** 刑事案件-[诈骗] 01040000 */
    ZHAPIAN("01040000"),
    /** 刑事案件-[敲诈勒索] 01050000 */
    QIAOZHAOLESUO("01050000"),
    /** 刑事案件-[伤害] 01060000 */
    SHANGHAI("01060000"),
    /** 刑事案件-[杀人] 01070000 */
    SHAREN("01070000"),
    /** 刑事案件-[强奸] 01080000 */
    QIANGJIAN("01080000"),
    /** 刑事案件-[涉毒案件] 01090000 */
    SHEDUANJIAN("01090000"),

    /** 刑事案件 01000000 */
    XSAJ("01000000"),
    /** 治安案件 02000000 */
    ZHIAN("02000000"),
    /** 群众求助 03000000 */
    QUNZHONG("03000000"),
    /** 交通类警情 04000000 */
    JIAOTONG("04000000"),
    /** 火灾事故 05000000 */
    HUOZAI("05000000"),
    /** 举报投诉 06000000 */
    JUBAOTOUSU("06000000"),
    /** 社会联动 07000000 */
    SHEHUI("07000000"),
    /** 群体性事件 08000000 */
    QUNTI("08000000"),
    /** 灾害事故 09000000 */
    ZAIHAI("09000000"),
    /** 行政违法 10000000 */
    XINGZHENG("10000000"),
    /** 其他警情 90000000 */
    QITA("90000000"),


    /** 报警求助 */
    BAOJINGQIUZHU("01"),
    /** 群众投诉 */
    QUNZHONGTOUSU("02"),
    /** 社会联动 */
    SHEHUILIANDONG("03"),
    /** 处警反馈 */
    CHUJINGFANKUI("04"),
    /** 工作咨询 */
    GONGZUOZIXUN("05"),
    /** 虚假警情 */
    XUJIAJINGQING("06"),
    /** 无理要求 */
    WULIYAOQIU("07"),
    /** 重复报警 */
    CHONGFUBAOJING("08"),
    /** 骚扰电话 */
    SAORAODIANHUA("09"),
    /** 系统测试 */
    XITONGCESHI("10"),
    /** 其它类型 */
    QITALEIXING("99"),
    /** 无效 */
    WUXIAO("90");




    private String value;

    ReportEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}