package com.dadovicn.enums;

/**
 * 系统字典枚举类
 * @author dadovicn
 * @date   2018/10/2
 */
public enum  SysDictEnum {

    GENDER("gender");

    private String value;

    private SysDictEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}