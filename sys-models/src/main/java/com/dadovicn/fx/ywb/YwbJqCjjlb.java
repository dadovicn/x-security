package com.dadovicn.fx.ywb;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "YWB_JQ_CJJLB", schema = "FX110", catalog = "")
public class YwbJqCjjlb {
    @Id
    @Column(name = "CJBH")
    private String cjbh;
    @Basic
    @Column(name = "JJBH")
    private String jjbh;
    @Basic
    @Column(name = "CJRGH")
    private String cjrgh;
    @Basic
    @Column(name = "CJRXM")
    private String cjrxm;
    @Basic
    @Column(name = "CJDWMC")
    private String cjdwmc;
    @Basic
    @Column(name = "CJNBDM")
    private String cjnbdm;
    @Basic
    @Column(name = "CJNBMC")
    private String cjnbmc;
    @Basic
    @Column(name = "CJSJDWMC")
    private String cjsjdwmc;
    @Basic
    @Column(name = "CJSJNBDM")
    private String cjsjnbdm;
    @Basic
    @Column(name = "CJSJNBMC")
    private String cjsjnbmc;
    @Basic
    @Column(name = "CJZT")
    private String cjzt;
    @Basic
    @Column(name = "SCSJ")
    private String scsj;
    @Basic
    @Column(name = "PFSJ")
    private LocalDateTime pfsj;
    @Basic
    @Column(name = "DDSJ")
    private LocalDateTime ddsj;
    @Basic
    @Column(name = "SLSJ")
    private LocalDateTime slsj;
    @Basic
    @Column(name = "SLDWMC")
    private String sldwmc;
    @Basic
    @Column(name = "SLNBDM")
    private String slnbdm;
    @Basic
    @Column(name = "SLNBMC")
    private String slnbmc;
    @Basic
    @Column(name = "SLSJDWMC")
    private String slsjdwmc;
    @Basic
    @Column(name = "SLSJNBDM")
    private String slsjnbdm;
    @Basic
    @Column(name = "SLSJNBMC")
    private String slsjnbmc;
    @Basic
    @Column(name = "SLRGH")
    private String slrgh;
    @Basic
    @Column(name = "SLRXM")
    private String slrxm;
    @Basic
    @Column(name = "SFCSSL")
    private String sfcssl;
    @Basic
    @Column(name = "SFGXZY")
    private String sfgxzy;
    @Basic
    @Column(name = "SFZYDH")
    private String sfzydh;
    @Basic
    @Column(name = "GXBQ")
    private String gxbq;
    @Basic
    @Column(name = "SFSDBJ")
    private String sfsdbj;
    @Basic
    @Column(name = "DQBJZD")
    private String dqbjzd;
    @Basic
    @Column(name = "TSGN")
    private String tsgn;
    @Basic
    @Column(name = "BZ")
    private String bz;
    @Basic
    @Column(name = "SFZF")
    private String sfzf;
    @Basic
    @Column(name = "DQZT")
    private String dqzt;
}