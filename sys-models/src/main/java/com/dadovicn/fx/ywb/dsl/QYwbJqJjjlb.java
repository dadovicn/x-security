package com.dadovicn.fx.ywb.dsl;

import com.dadovicn.fx.ywb.YwbJqJjjlb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import java.time.LocalDateTime;
import java.util.Date;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QYwbJqJjjlb is a Querydsl query type for YwbJqJjjlb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QYwbJqJjjlb extends EntityPathBase<YwbJqJjjlb> {

    private static final long serialVersionUID = -449872864L;

    public static final QYwbJqJjjlb ywbJqJjjlb = new QYwbJqJjjlb("ywbJqJjjlb");

    public final StringPath ajjbdm = createString("ajjbdm");

    public final StringPath ajjbmc = createString("ajjbmc");

    public final StringPath bjbzdm = createString("bjbzdm");

    public final StringPath bjdh = createString("bjdh");

    public final StringPath bjhm = createString("bjhm");

    public final StringPath bjnr = createString("bjnr");

    public final StringPath bjrlxdh = createString("bjrlxdh");

    public final StringPath bjrlxdz = createString("bjrlxdz");

    public final StringPath bjrxm = createString("bjrxm");

    public final DatePath<LocalDateTime> bjsj = createDate("bjsj", LocalDateTime.class);

    public final StringPath bjxsdm = createString("bjxsdm");

    public final StringPath bjxsmc = createString("bjxsmc");

    public final StringPath bz = createString("bz");

    public final StringPath cfbj = createString("cfbj");

    public final StringPath cjbs = createString("cjbs");

    public final StringPath dhyhdm = createString("dhyhdm");

    public final StringPath dhyhdz = createString("dhyhdz");

    public final StringPath dhyhhz = createString("dhyhhz");

    public final StringPath dqbjzd = createString("dqbjzd");

    public final StringPath dwzbx = createString("dwzbx");

    public final StringPath dwzby = createString("dwzby");

    public final StringPath dxbjlb = createString("dxbjlb");

    public final StringPath dxbjlx = createString("dxbjlx");

    public final StringPath dxbjxl = createString("dxbjxl");

    public final StringPath dxbjxx = createString("dxbjxx");

    public final StringPath dxlbmc = createString("dxlbmc");

    public final StringPath dxlxmc = createString("dxlxmc");

    public final StringPath dxxlmc = createString("dxxlmc");

    public final StringPath dxxxmc = createString("dxxxmc");

    public final StringPath fadz = createString("fadz");

    public final StringPath fadzcundm = createString("fadzcundm");

    public final StringPath fadzshimc = createString("fadzshimc");

    public final StringPath fadzzhendm = createString("fadzzhendm");

    public final StringPath fadzzhenmc = createString("fadzzhenmc");

    public final StringPath gljjbh = createString("gljjbh");

    public final StringPath gxdwmc = createString("gxdwmc");

    public final StringPath gxnbdm = createString("gxnbdm");

    public final StringPath gxnbmc = createString("gxnbmc");

    public final StringPath hjlsh = createString("hjlsh");

    public final StringPath jjbh = createString("jjbh");

    public final StringPath jjbjlb = createString("jjbjlb");

    public final StringPath jjbjlx = createString("jjbjlx");

    public final StringPath jjbjxl = createString("jjbjxl");

    public final StringPath jjbjxx = createString("jjbjxx");

    public final StringPath jjdwmc = createString("jjdwmc");

    public final StringPath jjlbmc = createString("jjlbmc");

    public final StringPath jjlxmc = createString("jjlxmc");

    public final StringPath jjnbdm = createString("jjnbdm");

    public final StringPath jjnbmc = createString("jjnbmc");

    public final StringPath jjrgh = createString("jjrgh");

    public final StringPath jjrxm = createString("jjrxm");

    public final StringPath jjxlmc = createString("jjxlmc");

    public final StringPath jjxxmc = createString("jjxxmc");

    public final StringPath jjzt = createString("jjzt");

    public final StringPath jqbqdm = createString("jqbqdm");

    public final StringPath jqbqmc = createString("jqbqmc");

    public final StringPath lhlbdm = createString("lhlbdm");

    public final StringPath lhlbmc = createString("lhlbmc");

    public final StringPath scsj = createString("scsj");

    public final StringPath sfrgsc = createString("sfrgsc");

    public final StringPath sfsdbj = createString("sfsdbj");

    public final StringPath tsgn = createString("tsgn");

    public final StringPath xqdm = createString("xqdm");

    public final StringPath xqmc = createString("xqmc");

    public final StringPath zbjz = createString("zbjz");

    public final StringPath zbzr = createString("zbzr");

    public final StringPath bjrxb = createString("bjrxb");

    public QYwbJqJjjlb(String variable) {
        super(YwbJqJjjlb.class, forVariable(variable));
    }

    public QYwbJqJjjlb(Path<? extends YwbJqJjjlb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QYwbJqJjjlb(PathMetadata metadata) {
        super(YwbJqJjjlb.class, metadata);
    }

}

