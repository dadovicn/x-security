package com.dadovicn.fx.ywb;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "YWB_JQ_JJJLB", schema = "FX110", catalog = "")
public class YwbJqJjjlb {
    @Id
    @Column(name = "JJBH")
    private String jjbh;
    @Basic
    @Column(name = "JJDWMC")
    private String jjdwmc;
    @Basic
    @Column(name = "JJNBDM")
    private String jjnbdm;
    @Basic
    @Column(name = "JJNBMC")
    private String jjnbmc;
    @Basic
    @Column(name = "BJXSDM")
    private String bjxsdm;
    @Basic
    @Column(name = "BJXSMC")
    private String bjxsmc;
    @Basic
    @Column(name = "LHLBDM")
    private String lhlbdm;
    @Basic
    @Column(name = "LHLBMC")
    private String lhlbmc;
    @Basic
    @Column(name = "SCSJ")
    private String scsj;
    @Basic
    @Column(name = "BJSJ")
    private LocalDateTime bjsj;
    @Basic
    @Column(name = "SFRGSC")
    private String sfrgsc;
    @Basic
    @Column(name = "JJZT")
    private String jjzt;
    @Basic
    @Column(name = "CJBS")
    private String cjbs;
    @Basic
    @Column(name = "CFBJ")
    private String cfbj;
    @Basic
    @Column(name = "JJRGH")
    private String jjrgh;
    @Basic
    @Column(name = "JJRXM")
    private String jjrxm;
    @Basic
    @Column(name = "HJLSH")
    private String hjlsh;
    @Basic
    @Column(name = "GLJJBH")
    private String gljjbh;
    @Basic
    @Column(name = "BJDH")
    private String bjdh;
    @Basic
    @Column(name = "BJHM")
    private String bjhm;
    @Basic
    @Column(name = "DHYHDM")
    private String dhyhdm;
    @Basic
    @Column(name = "DHYHHZ")
    private String dhyhhz;
    @Basic
    @Column(name = "DHYHDZ")
    private String dhyhdz;
    @Basic
    @Column(name = "BJRXM")
    private String bjrxm;
    @Basic
    @Column(name = "BJRLXDH")
    private String bjrlxdh;
    @Basic
    @Column(name = "BJRLXDZ")
    private String bjrlxdz;
    @Basic
    @Column(name = "FADZ")
    private String fadz;
    @Basic
    @Column(name = "BJBZDM")
    private String bjbzdm;
    @Basic
    @Column(name = "BJNR")
    private String bjnr;
    @Basic
    @Column(name = "AJJBDM")
    private String ajjbdm;
    @Basic
    @Column(name = "AJJBMC")
    private String ajjbmc;
    @Basic
    @Column(name = "JJBJLB")
    private String jjbjlb;
    @Basic
    @Column(name = "JJBJLX")
    private String jjbjlx;
    @Basic
    @Column(name = "JJBJXL")
    private String jjbjxl;
    @Basic
    @Column(name = "JJBJXX")
    private String jjbjxx;
    @Basic
    @Column(name = "JJLBMC")
    private String jjlbmc;
    @Basic
    @Column(name = "JJLXMC")
    private String jjlxmc;
    @Basic
    @Column(name = "JJXLMC")
    private String jjxlmc;
    @Basic
    @Column(name = "JJXXMC")
    private String jjxxmc;
    @Basic
    @Column(name = "DXBJLB")
    private String dxbjlb;
    @Basic
    @Column(name = "DXBJLX")
    private String dxbjlx;
    @Basic
    @Column(name = "DXBJXL")
    private String dxbjxl;
    @Basic
    @Column(name = "DXBJXX")
    private String dxbjxx;
    @Basic
    @Column(name = "DXLBMC")
    private String dxlbmc;
    @Basic
    @Column(name = "DXLXMC")
    private String dxlxmc;
    @Basic
    @Column(name = "DXXLMC")
    private String dxxlmc;
    @Basic
    @Column(name = "DXXXMC")
    private String dxxxmc;
    @Basic
    @Column(name = "JQBQDM")
    private String jqbqdm;
    @Basic
    @Column(name = "JQBQMC")
    private String jqbqmc;
    @Basic
    @Column(name = "GXDWMC")
    private String gxdwmc;
    @Basic
    @Column(name = "GXNBDM")
    private String gxnbdm;
    @Basic
    @Column(name = "GXNBMC")
    private String gxnbmc;
    @Basic
    @Column(name = "XQDM")
    private String xqdm;
    @Basic
    @Column(name = "XQMC")
    private String xqmc;
    @Basic
    @Column(name = "DWZBX")
    private String dwzbx;
    @Basic
    @Column(name = "DWZBY")
    private String dwzby;
    @Basic
    @Column(name = "SFSDBJ")
    private String sfsdbj;
    @Basic
    @Column(name = "DQBJZD")
    private String dqbjzd;
    @Basic
    @Column(name = "TSGN")
    private String tsgn;
    @Basic
    @Column(name = "BZ")
    private String bz;
    @Basic
    @Column(name = "FADZZHENDM")
    private String fadzzhendm;
    @Basic
    @Column(name = "FADZCUNDM")
    private String fadzcundm;
    @Basic
    @Column(name = "FADZSHIMC")
    private String fadzshimc;
    @Basic
    @Column(name = "FADZZHENMC")
    private String fadzzhenmc;
    @Basic
    @Column(name = "ZBJZ")
    private String zbjz;
    @Basic
    @Column(name = "ZBZR")
    private String zbzr;
    @Basic
    @Column(name = "BJRXB")
    private String bjrxb;
}