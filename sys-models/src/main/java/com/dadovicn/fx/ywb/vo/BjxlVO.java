package com.dadovicn.fx.ywb.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 报警细类
 * @author dadovicn
 * @date   2018/10/3
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BjxlVO {
    private String bjxldm;
    private String bjxlmc;
}