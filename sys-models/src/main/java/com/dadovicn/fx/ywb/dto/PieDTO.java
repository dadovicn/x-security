package com.dadovicn.fx.ywb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 饼图
 * @author dadovicn
 * @date   2018/10/13
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PieDTO {
    private String item;
    private Long count;
}