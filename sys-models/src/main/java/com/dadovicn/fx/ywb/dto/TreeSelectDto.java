package com.dadovicn.fx.ywb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 树 tree - select 数据
 * @author dadovicn
 * @date   2018/10/3
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TreeSelectDto {
    private String value;
    private String label;
    private Boolean isLeaf;
}