package com.dadovicn.fx.ywb.vo;

import lombok.Data;

import java.time.LocalDate;

/**
 * 日期和数量
 * @author dadovicn
 * @date   2018/10/1
 */
@Data
public class DayAndCountVO {
    private String date;
    private Long commits;
    private Integer month;
    private Integer day;
    private String week;
    private boolean lastDay;
    private boolean lastWeek;

    public DayAndCountVO() {}

    public DayAndCountVO(String date, Long commits) {
        this.date = date;
        this.commits = commits;
    }

    public DayAndCountVO(String date, Long commits, Integer month, Integer day, String week, boolean lastDay, boolean lastWeek) {
        this.date = date;
        this.commits = commits;
        this.month = month;
        this.day = day;
        this.week = week;
        this.lastDay = lastDay;
        this.lastWeek = lastWeek;
    }

}