package com.dadovicn.fx.ywb.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @desc 字典查询对象
 * @author dadovicn
 * @date   2018/11/12
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DictQO {
    private Long parentId;
}