package com.dadovicn.fx.ywb.dsl;

import com.dadovicn.fx.ywb.YwbJqCjjlb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.core.types.dsl.TimePath;

import javax.annotation.Generated;

import java.time.LocalDateTime;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QYwbJqCjjlb is a Querydsl query type for YwbJqCjjlb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QYwbJqCjjlb extends EntityPathBase<YwbJqCjjlb> {

    private static final long serialVersionUID = -456337511L;

    public static final QYwbJqCjjlb ywbJqCjjlb = new QYwbJqCjjlb("ywbJqCjjlb");

    public final StringPath bz = createString("bz");

    public final StringPath cjbh = createString("cjbh");

    public final StringPath jjbh = createString("jjbh");

    public final StringPath cjdwmc = createString("cjdwmc");

    public final StringPath cjnbdm = createString("cjnbdm");

    public final StringPath cjnbmc = createString("cjnbmc");

    public final StringPath cjrgh = createString("cjrgh");

    public final StringPath cjrxm = createString("cjrxm");

    public final StringPath cjsjdwmc = createString("cjsjdwmc");

    public final StringPath cjsjnbdm = createString("cjsjnbdm");

    public final StringPath cjsjnbmc = createString("cjsjnbmc");

    public final StringPath cjzt = createString("cjzt");

    public final TimePath<java.sql.Time> ddsj = createTime("ddsj", java.sql.Time.class);

    public final StringPath dqbjzd = createString("dqbjzd");

    public final StringPath dqzt = createString("dqzt");

    public final StringPath gxbq = createString("gxbq");

    public final TimePath<LocalDateTime> pfsj = createTime("pfsj", LocalDateTime.class);

    public final StringPath scsj = createString("scsj");

    public final StringPath sfcssl = createString("sfcssl");

    public final StringPath sfgxzy = createString("sfgxzy");

    public final StringPath sfsdbj = createString("sfsdbj");

    public final StringPath sfzf = createString("sfzf");

    public final StringPath sfzydh = createString("sfzydh");

    public final StringPath sldwmc = createString("sldwmc");

    public final StringPath slnbdm = createString("slnbdm");

    public final StringPath slnbmc = createString("slnbmc");

    public final StringPath slrgh = createString("slrgh");

    public final StringPath slrxm = createString("slrxm");

    public final TimePath<LocalDateTime> slsj = createTime("slsj", LocalDateTime.class);

    public final StringPath slsjdwmc = createString("slsjdwmc");

    public final StringPath slsjnbdm = createString("slsjnbdm");

    public final StringPath slsjnbmc = createString("slsjnbmc");

    public final StringPath tsgn = createString("tsgn");

    public QYwbJqCjjlb(String variable) {
        super(YwbJqCjjlb.class, forVariable(variable));
    }

    public QYwbJqCjjlb(Path<? extends YwbJqCjjlb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QYwbJqCjjlb(PathMetadata metadata) {
        super(YwbJqCjjlb.class, metadata);
    }

}

