package com.dadovicn.fx.ywb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * 近一月趋势图数据源
 * @author dadovicn
 * @date   2018/10/19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecentMonthJjDTO {
    private Long jjCount;
    private String date;
}