package com.dadovicn.fx.ywb.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 字典值
 * @author dadovicn
 * @date   2018/10/2
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DictVO {
    private String dictKey;
    private String dictValue;
    private String dictDesc;
}