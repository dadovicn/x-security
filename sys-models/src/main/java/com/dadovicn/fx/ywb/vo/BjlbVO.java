package com.dadovicn.fx.ywb.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 报警类别
 * @author dadovicn
 * @date   2018/10/3
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BjlbVO {
    private String bjlbdm;
    private String bjlbmc;
}