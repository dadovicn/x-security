package com.dadovicn.fx.ywb.dsl;

import com.dadovicn.fx.ywb.YwbBjYjxxb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QYwbBjYjxxb is a Querydsl query type for YwbBjYjxxb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QYwbBjYjxxb extends EntityPathBase<YwbBjYjxxb> {

    private static final long serialVersionUID = 853494864L;

    public static final QYwbBjYjxxb ywbBjYjxxb = new QYwbBjYjxxb("ywbBjYjxxb");

    public final NumberPath<Long> hzsg = createNumber("hzsg", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> jbts = createNumber("jbts", Long.class);

    public final NumberPath<Long> jtljq = createNumber("jtljq", Long.class);

    public final NumberPath<Long> qtjq = createNumber("qtjq", Long.class);

    public final NumberPath<Long> qtxsj = createNumber("qtxsj", Long.class);

    public final NumberPath<Long> qzqz = createNumber("qzqz", Long.class);

    public final NumberPath<Long> shld = createNumber("shld", Long.class);

    public final StringPath type = createString("type");

    public final NumberPath<Long> xsaj = createNumber("xsaj", Long.class);

    public final NumberPath<Long> xzwf = createNumber("xzwf", Long.class);

    public final NumberPath<Long> zaaj = createNumber("zaaj", Long.class);

    public final NumberPath<Long> zhsg = createNumber("zhsg", Long.class);

    public QYwbBjYjxxb(String variable) {
        super(YwbBjYjxxb.class, forVariable(variable));
    }

    public QYwbBjYjxxb(Path<? extends YwbBjYjxxb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QYwbBjYjxxb(PathMetadata metadata) {
        super(YwbBjYjxxb.class, metadata);
    }

}

