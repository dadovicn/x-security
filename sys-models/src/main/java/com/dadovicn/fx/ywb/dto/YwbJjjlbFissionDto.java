package com.dadovicn.fx.ywb.dto;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 接警记录表裂变dto
 * @author dadovicn
 * @date   2018/9/12
 */
@Data
@ExcelTarget("ywbJjjlbFissionDto")
public class YwbJjjlbFissionDto {
    @Excel(name="报警人电话")
    private String bjrlxdh;
    @Excel(name="报警时间")
    private LocalDateTime bjsj;
    @Excel(name="案发地址")
    private String fadz;
    @Excel(name="接警摘要")
    private String bjnr;
    @Excel(name="报警人姓名")
    private String bjrxm;
    @Excel(name="接警单号码")
    private String jjbh;
    @Excel(name="接警类型名称")
    private String jjlbmc;
//    @Excel(name="接警列别描述")
    //private String jjlbmc;
//    @Excel(name="接警细类描述")
//    private String jjxzmc;
    @Excel(name="报警人性别")
    private String bjrxb;
    @Excel(name="处警人姓名")
    private String cjrxm;
    @Excel(name="出警编号")
    private String cjbh;
}