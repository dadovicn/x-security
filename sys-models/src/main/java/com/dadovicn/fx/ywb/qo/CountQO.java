package com.dadovicn.fx.ywb.qo;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class CountQO {
    private List<LocalDateTime> date;
}