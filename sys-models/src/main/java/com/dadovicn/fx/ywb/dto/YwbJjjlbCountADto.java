package com.dadovicn.fx.ywb.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class YwbJjjlbCountADto {
    private LocalDateTime localDateTime;
    private int count;
}