package com.dadovicn.fx.ywb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class YjxxDTO {
    /** 报警类别 */
    private String bjlb;
    /** 预警级别 */
    private Long yjjb;
    /** 阈值 */
    private Long sl;
    /** 类型 */
    private String type;
}