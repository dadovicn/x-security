package com.dadovicn.fx.ywb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 接处警详情数据
 * @author dadovicn
 * @date   2018/10/6
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class JcjDetailDTO {
    /** 接警编号 */
    private String jjbh;
    /** 来话类别名称 */
    private String lhlbmc;
    /** 报警时间 */
    private LocalDateTime bjsj;
    /** 是否人工生成 */
    private String sfrgsc;
    /** 接警状态 */
    private String jjzt;
    /** 出警标志 0 为出警, 1 一处静 */
    private String cjbs;
    /** 是否重复报警 cfbj 0 非重复, 1 重复 */
    private String cfbj;
    /** 接警人工号 */
    private String jjrgh;
    /** 接警人姓名 */
    private String jjrxm;
    /** 报警电话 */
    private String bjdh;
    /** 报警人联系电话 */
    private String bjrlxdh;
    /** 报警人姓名 */
    private String bjrxm;
    /** 报警人性别 */
    private String bjrxb;
    /** 发案地址 */
    private String fadz;
    /** 报警内容 */
    private String bjnr;
    /** 接警报警细类 */
    private String jjlbmc;
    /** 接警报警类型 */
    private String jjlxmc;
    /** 接警报警细类 */
    private String jjxlmc;
    /** 接警报警细细 */
    private String jjxxmc;
    /** 管辖单位名称 */
    private String gxdwmc;
    /** 辖区名称 */
    private String xqmc;

}