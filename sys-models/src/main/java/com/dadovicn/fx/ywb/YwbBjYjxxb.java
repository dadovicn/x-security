package com.dadovicn.fx.ywb;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "YWB_BJ_YJXXB", schema = "FX110", catalog = "")
public class YwbBjYjxxb {
    private long id;
    private Long xsaj;
    private Long zaaj;
    private Long qzqz;
    private Long jtljq;
    private Long hzsg;
    private Long jbts;
    private Long shld;
    private Long qtxsj;
    private Long zhsg;
    private Long xzwf;
    private Long qtjq;
    private String type;

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "XSAJ")
    public Long getXsaj() {
        return xsaj;
    }

    public void setXsaj(Long xsaj) {
        this.xsaj = xsaj;
    }

    @Basic
    @Column(name = "ZAAJ")
    public Long getZaaj() {
        return zaaj;
    }

    public void setZaaj(Long zaaj) {
        this.zaaj = zaaj;
    }

    @Basic
    @Column(name = "QZQZ")
    public Long getQzqz() {
        return qzqz;
    }

    public void setQzqz(Long qzqz) {
        this.qzqz = qzqz;
    }

    @Basic
    @Column(name = "JTLJQ")
    public Long getJtljq() {
        return jtljq;
    }

    public void setJtljq(Long jtljq) {
        this.jtljq = jtljq;
    }

    @Basic
    @Column(name = "HZSG")
    public Long getHzsg() {
        return hzsg;
    }

    public void setHzsg(Long hzsg) {
        this.hzsg = hzsg;
    }

    @Basic
    @Column(name = "JBTS")
    public Long getJbts() {
        return jbts;
    }

    public void setJbts(Long jbts) {
        this.jbts = jbts;
    }

    @Basic
    @Column(name = "SHLD")
    public Long getShld() {
        return shld;
    }

    public void setShld(Long shld) {
        this.shld = shld;
    }

    @Basic
    @Column(name = "QTXSJ")
    public Long getQtxsj() {
        return qtxsj;
    }

    public void setQtxsj(Long qtxsj) {
        this.qtxsj = qtxsj;
    }

    @Basic
    @Column(name = "ZHSG")
    public Long getZhsg() {
        return zhsg;
    }

    public void setZhsg(Long zhsg) {
        this.zhsg = zhsg;
    }

    @Basic
    @Column(name = "XZWF")
    public Long getXzwf() {
        return xzwf;
    }

    public void setXzwf(Long xzwf) {
        this.xzwf = xzwf;
    }

    @Basic
    @Column(name = "QTJQ")
    public Long getQtjq() {
        return qtjq;
    }

    public void setQtjq(Long qtjq) {
        this.qtjq = qtjq;
    }

    @Basic
    @Column(name = "TYPE")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}