package com.dadovicn.fx.ywb.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Coordinate {
    /** x 轴值 */
    private String x;
    /** y 轴值 */
    private Long y;
}