package com.dadovicn.fx.ywb.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 报表结果对象
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReportVO {
    /** 接警总量 */
    private Long jjTotal;
    /** 有效警情合计 */
    private Long validTotal;
    /** 刑事案件合计 */
    private Long xsajTotal;
    /** 刑事案件-[盗窃] 01010000 */
    private Long daoqie;
    /** 刑事案件-[抢劫] 01020000 */
    private Long qiangjie;
    /** 刑事案件-[抢夺] 01030000 */
    private Long qiangduo;
    /** 刑事案件-[诈骗] 01040000 */
    private Long zhapian;
    /** 刑事案件-[敲诈勒索] 01050000 */
    private Long qiaozhalesuo;
    /** 刑事案件-[伤害] 01060000 */
    private Long shanghai;
    /** 刑事案件-[杀人] 01070000 */
    private Long sharen;
    /** 刑事案件-[强奸] 01080000 */
    private Long qiangjian;
    /** 刑事案件-[涉毒案件] 01090000 */
    private Long sheduanjian;
    /** 其他刑事案件 */
    private Long qitaxingshi;

    /** 治安案件 02000000 */
    private Long zhian;
    /** 群众求助 03000000 */
    private Long qunzhong;
    /** 交通类警情 04000000 */
    private Long jiaotong;
    /** 火灾事故 05000000 */
    private Long huozai;
    /** 举报投诉 06000000 */
    private Long jubaotousu;
    /** 社会联动 07000000 */
    private Long shehui;
    /** 群体性事件 08000000 */
    private Long qunti;
    /** 灾害事故 09000000 */
    private Long zaihai;
    /** 行政违法 10000000 */
    private Long xingzheng;
    /** 其他警情 90000000 */
    private Long qita;

    /** 无效警情 = 总量 - 有效 */
    private Long inValidTotal;

    /** 01 */
    private Long baojingqiuzhu;
    /** 02 */
    private Long qunzhongtousu;
    /** 03 */
    private Long shehuiliandong;
    /** 04 */
    private Long chujingfankui;
    /** 05 */
    private Long gongzuozixun;
    /** 06 */
    private Long xujiajingqing;
    /** 07 */
    private Long wuliyaoqiu;
    /** 08 */
    private Long chongfubaojing;
    /** 09 */
    private Long saoraodianhua;
    /** 10 */
    private Long xitongceshi;
    /** 99 */
    private Long qitaleixing;
    /** 90 */
    private Long wuxiao;
    /** 来话类别总量 = 接警总量*/
    private Long lhlbTotal;


}