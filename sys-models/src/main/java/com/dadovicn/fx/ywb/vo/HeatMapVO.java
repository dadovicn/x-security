package com.dadovicn.fx.ywb.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 日期和数量
 * @author dadovicn
 * @date   2018/10/1
 */
@Data
@NoArgsConstructor
public class HeatMapVO {
    private String day;
    private String jjlbmc;
    private Long count;

    public HeatMapVO(String jjlbmc, Long count) {
        this.jjlbmc = jjlbmc;
        this.count =count;
    }

    public HeatMapVO(String day, String jjlbmc, Long count) {
        this.day = day;
        this.jjlbmc = jjlbmc;
        this.count = count;
    }
}