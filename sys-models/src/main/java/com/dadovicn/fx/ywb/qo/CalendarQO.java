package com.dadovicn.fx.ywb.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CalendarQO {
    private List<String> bjlbdmList;
    private String xqdm;
}