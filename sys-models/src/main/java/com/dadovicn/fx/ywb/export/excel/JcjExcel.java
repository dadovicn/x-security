package com.dadovicn.fx.ywb.export.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 接处警导出表
 * @author dadovicn
 * @date   2018/9/15
 */
@ExcelTarget("jcjExcel")
@Data
public class JcjExcel {
    @Excel(name = "报警人电话")
    private String bjrlxdh;
    @Excel(name = "报警时间" ,databaseFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd")
    private LocalDateTime bjsj;
    @Excel(name = "案发地址")
    private String fadz;
    @Excel(name = "接警摘要")
    private String bjnr;
    @Excel(name = "报警人姓名")
    private String bjrxm;
    @Excel(name = "接警单号码")
    private String jjbh;
    @Excel(name = "接警类型名称")
    private String jjlbmc;
//    @Excel(name = "接警列别描述")
//    private String jjlbmc;
//    @Excel(name = "接警细类描述")
//    private String jjxzmc;
    @Excel(name = "报警人性别")
    private String bjrxb;
    @Excel(name = "处警人姓名")
    private String cjrxm;
    @Excel(name = "出警编号")
    private String cjbh;
}