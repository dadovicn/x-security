package com.dadovicn.fx.ywb.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class XqVO {
    private String xqdm;
    private String xqmc;
}