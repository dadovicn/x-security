package com.dadovicn.fx.ywb.qo;

import com.dadovicn.annoation.*;
import com.dadovicn.fx.ywb.dsl.QYwbJqCjjlb;
import com.dadovicn.fx.ywb.dsl.QYwbJqJjjlb;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 接处警表单查询
 * @author dadovicn
 * @date   2018/9/11
 */
@Data
@ToString
public class JcjQO extends BaseQO{
    @ForClass(classPairs = {QYwbJqJjjlb.class, QYwbJqCjjlb.class})
    private String jjbh;

    /** 报警人姓名 */
    @And
    @ForClass(name = "QYwbJqJjjlb")
    private String bjrxm;

    /** 报警人联系电话 */
    @And
    @ForClass(name = "QYwbJqJjjlb")
    private String bjrlxdh;

    /** 案发地址 */
    @And
    @ForClass(name = "QYwbJqJjjlb")
    private String fadz;

    /** 报警内容 */
    @And
    @ForClass(name = "QYwbJqJjjlb")
    private String bjnr;

    /** 接警员姓名 */
    @And
    @ForClass(name = "QYwbJqCjjlb")
    private String jjrxm;

    /** 开始结束时间 */
    @Date("bjsj")
    @ForClass(name = "QYwbJqJjjlb")
    private List<LocalDateTime> date;

    @And
    @ForClass(name = "QYwbJqCjjlb")
    private String cjdwmc;

    @And
    @ForClass(name = "QYwbJqCjjlb")
    private String cjzt;
}