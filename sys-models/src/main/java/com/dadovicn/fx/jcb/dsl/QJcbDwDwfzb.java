package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbDwDwfzb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbDwDwfzb is a Querydsl query type for JcbDwDwfzb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbDwDwfzb extends EntityPathBase<JcbDwDwfzb> {

    private static final long serialVersionUID = -304686203L;

    public static final QJcbDwDwfzb jcbDwDwfzb = new QJcbDwDwfzb("jcbDwDwfzb");

    public final StringPath bz = createString("bz");

    public final StringPath dwfzdm = createString("dwfzdm");

    public final StringPath dwfzmc = createString("dwfzmc");

    public final StringPath nbdwfzdm = createString("nbdwfzdm");

    public final StringPath pxbh = createString("pxbh");

    public QJcbDwDwfzb(String variable) {
        super(JcbDwDwfzb.class, forVariable(variable));
    }

    public QJcbDwDwfzb(Path<? extends JcbDwDwfzb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbDwDwfzb(PathMetadata metadata) {
        super(JcbDwDwfzb.class, metadata);
    }

}

