package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjBjlbb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * 报警类别
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjBjlbb extends EntityPathBase<JcbBjBjlbb> {

    private static final long serialVersionUID = 1840865465L;

    public static final QJcbBjBjlbb jcbBjBjlbb = new QJcbBjBjlbb("jcbBjBjlbb");

    public final StringPath bjlbdm = createString("bjlbdm");

    public final StringPath bjlbmc = createString("bjlbmc");

    public final StringPath bz = createString("bz");

    public final StringPath lbysdm = createString("lbysdm");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath tsgn = createString("tsgn");

    public QJcbBjBjlbb(String variable) {
        super(JcbBjBjlbb.class, forVariable(variable));
    }

    public QJcbBjBjlbb(Path<? extends JcbBjBjlbb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjBjlbb(PathMetadata metadata) {
        super(JcbBjBjlbb.class, metadata);
    }

}

