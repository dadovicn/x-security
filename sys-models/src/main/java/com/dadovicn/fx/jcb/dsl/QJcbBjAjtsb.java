package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjAjtsb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjAjtsb is a Querydsl query type for JcbBjAjtsb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjAjtsb extends EntityPathBase<JcbBjAjtsb> {

    private static final long serialVersionUID = 1839950159L;

    public static final QJcbBjAjtsb jcbBjAjtsb = new QJcbBjAjtsb("jcbBjAjtsb");

    public final StringPath ajdm = createString("ajdm");

    public final StringPath ajtsdm = createString("ajtsdm");

    public final StringPath bz = createString("bz");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath tsnr = createString("tsnr");

    public QJcbBjAjtsb(String variable) {
        super(JcbBjAjtsb.class, forVariable(variable));
    }

    public QJcbBjAjtsb(Path<? extends JcbBjAjtsb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjAjtsb(PathMetadata metadata) {
        super(JcbBjAjtsb.class, metadata);
    }

}

