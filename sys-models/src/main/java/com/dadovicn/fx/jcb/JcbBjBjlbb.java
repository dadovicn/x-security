package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

/**
 * 类别
 * @author dadovicn
 * @date   2018/10/3
 */
@Entity
@Data
@Table(name = "JCB_BJ_BJLBB", schema = "FX110", catalog = "")
public class JcbBjBjlbb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "BJLBDM")
    private String bjlbdm;
    @Basic
    @Column(name = "BJLBMC")
    private String bjlbmc;
    @Basic
    @Column(name = "LBYSDM")
    private String lbysdm;
    @Basic
    @Column(name = "TSGN")
    private String tsgn;
    @Basic
    @Column(name = "BZ")
    private String bz;
}