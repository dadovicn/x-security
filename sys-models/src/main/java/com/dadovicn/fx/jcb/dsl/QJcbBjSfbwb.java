package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjSfbwb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjSfbwb is a Querydsl query type for JcbBjSfbwb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjSfbwb extends EntityPathBase<JcbBjSfbwb> {

    private static final long serialVersionUID = 1856437199L;

    public static final QJcbBjSfbwb jcbBjSfbwb = new QJcbBjSfbwb("jcbBjSfbwb");

    public final StringPath bz = createString("bz");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath sfbwdm = createString("sfbwdm");

    public final StringPath sfbwmc = createString("sfbwmc");

    public final StringPath sjsfbw = createString("sjsfbw");

    public QJcbBjSfbwb(String variable) {
        super(JcbBjSfbwb.class, forVariable(variable));
    }

    public QJcbBjSfbwb(Path<? extends JcbBjSfbwb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjSfbwb(PathMetadata metadata) {
        super(JcbBjSfbwb.class, metadata);
    }

}

