package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjSfdzb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjSfdzb is a Querydsl query type for JcbBjSfdzb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjSfdzb extends EntityPathBase<JcbBjSfdzb> {

    private static final long serialVersionUID = 1856439214L;

    public static final QJcbBjSfdzb jcbBjSfdzb = new QJcbBjSfdzb("jcbBjSfdzb");

    public final StringPath bz = createString("bz");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath sfdzdm = createString("sfdzdm");

    public final StringPath sfdzmc = createString("sfdzmc");

    public final StringPath sjsfdz = createString("sjsfdz");

    public QJcbBjSfdzb(String variable) {
        super(JcbBjSfdzb.class, forVariable(variable));
    }

    public QJcbBjSfdzb(Path<? extends JcbBjSfdzb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjSfdzb(PathMetadata metadata) {
        super(JcbBjSfdzb.class, metadata);
    }

}

