package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

/**
 * 类型
 */
@Entity
@Data
@Table(name = "JCB_BJ_BJLXB", schema = "FX110", catalog = "")
public class JcbBjBjlxb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "BJLXDM")
    private String bjlxdm;
    @Basic
    @Column(name = "BJLXMC")
    private String bjlxmc;
    @Basic
    @Column(name = "BJLBDM")
    private String bjlbdm;
    @Basic
    @Column(name = "LXYSDM")
    private String lxysdm;
    @Basic
    @Column(name = "TSGN")
    private String tsgn;
    @Basic
    @Column(name = "BZ")
    private String bz;
}