package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbDwDwb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbDwDwb is a Querydsl query type for JcbDwDwb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbDwDwb extends EntityPathBase<JcbDwDwb> {

    private static final long serialVersionUID = 115883929L;

    public static final QJcbDwDwb jcbDwDwb = new QJcbDwDwb("jcbDwDwb");

    public final StringPath bz = createString("bz");

    public final StringPath dataip = createString("dataip");

    public final StringPath datapwd = createString("datapwd");

    public final StringPath datauser = createString("datauser");

    public final StringPath dhhm = createString("dhhm");

    public final StringPath dwdm = createString("dwdm");

    public final StringPath dwmc = createString("dwmc");

    public final StringPath dwnjgx = createString("dwnjgx");

    public final StringPath nbdwdm = createString("nbdwdm");

    public final StringPath nbdwmc = createString("nbdwmc");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath sjdwdm = createString("sjdwdm");

    public final StringPath tsgn = createString("tsgn");

    public final StringPath tsgnKz = createString("tsgnKz");

    public final StringPath zb = createString("zb");

    public QJcbDwDwb(String variable) {
        super(JcbDwDwb.class, forVariable(variable));
    }

    public QJcbDwDwb(Path<? extends JcbDwDwb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbDwDwb(PathMetadata metadata) {
        super(JcbDwDwb.class, metadata);
    }

}

