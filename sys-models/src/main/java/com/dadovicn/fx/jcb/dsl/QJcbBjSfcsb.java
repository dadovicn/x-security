package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjSfcsb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjSfcsb is a Querydsl query type for JcbBjSfcsb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjSfcsb extends EntityPathBase<JcbBjSfcsb> {

    private static final long serialVersionUID = 1856438036L;

    public static final QJcbBjSfcsb jcbBjSfcsb = new QJcbBjSfcsb("jcbBjSfcsb");

    public final StringPath bz = createString("bz");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath sfcsdm = createString("sfcsdm");

    public final StringPath sfcsmc = createString("sfcsmc");

    public final StringPath sjsfcs = createString("sjsfcs");

    public QJcbBjSfcsb(String variable) {
        super(JcbBjSfcsb.class, forVariable(variable));
    }

    public QJcbBjSfcsb(Path<? extends JcbBjSfcsb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjSfcsb(PathMetadata metadata) {
        super(JcbBjSfcsb.class, metadata);
    }

}

