package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjBjxlb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * 报警细类
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjBjxlb extends EntityPathBase<JcbBjBjxlb> {

    private static final long serialVersionUID = 1840877307L;

    public static final QJcbBjBjxlb jcbBjBjxlb = new QJcbBjBjxlb("jcbBjBjxlb");

    public final StringPath bjxldm = createString("bjxldm");

    public final StringPath bjxlmc = createString("bjxlmc");

    public final StringPath bjlxdm = createString("bjlxdm");

    public final StringPath bz = createString("bz");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath tsgn = createString("tsgn");

    public final StringPath xlysdm = createString("xlysdm");

    public QJcbBjBjxlb(String variable) {
        super(JcbBjBjxlb.class, forVariable(variable));
    }

    public QJcbBjBjxlb(Path<? extends JcbBjBjxlb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjBjxlb(PathMetadata metadata) {
        super(JcbBjBjxlb.class, metadata);
    }

}

