package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjBjxxb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjBjxxb is a Querydsl query type for JcbBjBjxxb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjBjxxb extends EntityPathBase<JcbBjBjxxb> {

    private static final long serialVersionUID = 1840877679L;

    public static final QJcbBjBjxxb jcbBjBjxxb = new QJcbBjBjxxb("jcbBjBjxxb");

    public final StringPath bjxxdm = createString("bjxxdm");

    public final StringPath bjxxmc = createString("bjxxmc");

    public final StringPath bz = createString("bz");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath tsgn = createString("tsgn");

    public final StringPath xxysdm = createString("xxysdm");

    public QJcbBjBjxxb(String variable) {
        super(JcbBjBjxxb.class, forVariable(variable));
    }

    public QJcbBjBjxxb(Path<? extends JcbBjBjxxb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjBjxxb(PathMetadata metadata) {
        super(JcbBjBjxxb.class, metadata);
    }

}

