package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbDwDwlbb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbDwDwlbb is a Querydsl query type for JcbDwDwlbb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbDwDwlbb extends EntityPathBase<JcbDwDwlbb> {

    private static final long serialVersionUID = -304681181L;

    public static final QJcbDwDwlbb jcbDwDwlbb = new QJcbDwDwlbb("jcbDwDwlbb");

    public final StringPath bz = createString("bz");

    public final StringPath dwlbdm = createString("dwlbdm");

    public final StringPath dwlbmc = createString("dwlbmc");

    public final StringPath pxbh = createString("pxbh");

    public QJcbDwDwlbb(String variable) {
        super(JcbDwDwlbb.class, forVariable(variable));
    }

    public QJcbDwDwlbb(Path<? extends JcbDwDwlbb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbDwDwlbb(PathMetadata metadata) {
        super(JcbDwDwlbb.class, metadata);
    }

}

