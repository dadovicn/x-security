package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_DW_DWLBB", schema = "FX110", catalog = "")
public class JcbDwDwlbb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "DWLBDM")
    private String dwlbdm;
    @Basic
    @Column(name = "DWLBMC")
    private String dwlbmc;
    @Basic
    @Column(name = "BZ")
    private String bz;
}