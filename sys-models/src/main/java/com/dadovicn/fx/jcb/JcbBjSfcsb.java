package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data

@Table(name = "JCB_BJ_SFCSB", schema = "FX110", catalog = "")
public class JcbBjSfcsb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "SFCSDM")
    private String sfcsdm;
    @Basic
    @Column(name = "SFCSMC")
    private String sfcsmc;
    @Basic
    @Column(name = "SJSFCS")
    private String sjsfcs;
    @Basic
    @Column(name = "BZ")
    private String bz;
}