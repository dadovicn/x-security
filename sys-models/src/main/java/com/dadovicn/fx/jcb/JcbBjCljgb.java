package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_CLJGB", schema = "FX110", catalog = "")
public class JcbBjCljgb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;

    @Id
    @Column(name = "CLJGDM")
    private String cljgdm;

    @Basic
    @Column(name = "CLJGMC")
    private String cljgmc;

    @Basic
    @Column(name = "BZ")
    private String bz;
}