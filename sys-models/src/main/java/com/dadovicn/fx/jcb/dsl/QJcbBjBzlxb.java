package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjBzlxb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjBzlxb is a Querydsl query type for JcbBjBzlxb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjBzlxb extends EntityPathBase<JcbBjBzlxb> {

    private static final long serialVersionUID = 1841342803L;

    public static final QJcbBjBzlxb jcbBjBzlxb = new QJcbBjBzlxb("jcbBjBzlxb");

    public final StringPath bz = createString("bz");

    public final StringPath bzlxdm = createString("bzlxdm");

    public final StringPath bzlxmc = createString("bzlxmc");

    public final StringPath pxbh = createString("pxbh");

    public QJcbBjBzlxb(String variable) {
        super(JcbBjBzlxb.class, forVariable(variable));
    }

    public QJcbBjBzlxb(Path<? extends JcbBjBzlxb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjBzlxb(PathMetadata metadata) {
        super(JcbBjBzlxb.class, metadata);
    }

}

