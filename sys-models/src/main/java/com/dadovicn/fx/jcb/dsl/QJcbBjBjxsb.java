package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjBjxsb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjBjxsb is a Querydsl query type for JcbBjBjxsb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjBjxsb extends EntityPathBase<JcbBjBjxsb> {

    private static final long serialVersionUID = 1840877524L;

    public static final QJcbBjBjxsb jcbBjBjxsb = new QJcbBjBjxsb("jcbBjBjxsb");

    public final StringPath bjxsdm = createString("bjxsdm");

    public final StringPath bjxsmc = createString("bjxsmc");

    public final StringPath bz = createString("bz");

    public final StringPath pxbh = createString("pxbh");

    public QJcbBjBjxsb(String variable) {
        super(JcbBjBjxsb.class, forVariable(variable));
    }

    public QJcbBjBjxsb(Path<? extends JcbBjBjxsb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjBjxsb(PathMetadata metadata) {
        super(JcbBjBjxsb.class, metadata);
    }

}

