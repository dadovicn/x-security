package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjJqbqb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjJqbqb is a Querydsl query type for JcbBjJqbqb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjJqbqb extends EntityPathBase<JcbBjJqbqb> {

    private static final long serialVersionUID = 1848453025L;

    public static final QJcbBjJqbqb jcbBjJqbqb = new QJcbBjJqbqb("jcbBjJqbqb");

    public final StringPath bz = createString("bz");

    public final StringPath jqbqdm = createString("jqbqdm");

    public final StringPath jqbqmc = createString("jqbqmc");

    public final StringPath pxbh = createString("pxbh");

    public QJcbBjJqbqb(String variable) {
        super(JcbBjJqbqb.class, forVariable(variable));
    }

    public QJcbBjJqbqb(Path<? extends JcbBjJqbqb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjJqbqb(PathMetadata metadata) {
        super(JcbBjJqbqb.class, metadata);
    }

}

