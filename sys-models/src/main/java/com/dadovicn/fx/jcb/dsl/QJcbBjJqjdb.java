package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjJqjdb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjJqjdb is a Querydsl query type for JcbBjJqjdb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjJqjdb extends EntityPathBase<JcbBjJqjdb> {

    private static final long serialVersionUID = 1848460310L;

    public static final QJcbBjJqjdb jcbBjJqjdb = new QJcbBjJqjdb("jcbBjJqjdb");

    public final StringPath bz = createString("bz");

    public final StringPath jqjddm = createString("jqjddm");

    public final StringPath jqjdmc = createString("jqjdmc");

    public final StringPath pxbh = createString("pxbh");

    public QJcbBjJqjdb(String variable) {
        super(JcbBjJqjdb.class, forVariable(variable));
    }

    public QJcbBjJqjdb(Path<? extends JcbBjJqjdb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjJqjdb(PathMetadata metadata) {
        super(JcbBjJqjdb.class, metadata);
    }

}

