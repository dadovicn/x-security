package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjBjlxb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * 报警类型
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjBjlxb extends EntityPathBase<JcbBjBjlxb> {

    private static final long serialVersionUID = 1840866147L;

    public static final QJcbBjBjlxb jcbBjBjlxb = new QJcbBjBjlxb("jcbBjBjlxb");

    public final StringPath bjlxdm = createString("bjlxdm");

    public final StringPath bjlxmc = createString("bjlxmc");

    public final StringPath bjlbdm = createString("bjlbdm");

    public final StringPath bz = createString("bz");

    public final StringPath lxysdm = createString("lxysdm");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath tsgn = createString("tsgn");

    public QJcbBjBjlxb(String variable) {
        super(JcbBjBjlxb.class, forVariable(variable));
    }

    public QJcbBjBjlxb(Path<? extends JcbBjBjlxb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjBjlxb(PathMetadata metadata) {
        super(JcbBjBjlxb.class, metadata);
    }

}

