package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_XQDWB", schema = "FX110", catalog = "")
public class JcbBjXqdwb {
    @Id
    @Column(name = "GUID")
    private String guid;
    @Basic
    @Column(name = "XQDM")
    private String xqdm;
    @Basic
    @Column(name = "XQMC")
    private String xqmc;
    @Basic
    @Column(name = "DWMC")
    private String dwmc;
    @Basic
    @Column(name = "NBDM")
    private String nbdm;
    @Basic
    @Column(name = "NBMC")
    private String nbmc;
}