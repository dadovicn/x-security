package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_JQBQB", schema = "FX110", catalog = "")
public class JcbBjJqbqb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;

    @Id
    @Column(name = "JQBQDM")
    private String jqbqdm;

    @Basic
    @Column(name = "JQBQMC")
    private String jqbqmc;

    @Basic
    @Column(name = "BZ")
    private String bz;
}