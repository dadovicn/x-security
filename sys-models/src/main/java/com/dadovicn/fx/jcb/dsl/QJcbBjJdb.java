package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjJdb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjJdb is a Querydsl query type for JcbBjJdb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjJdb extends EntityPathBase<JcbBjJdb> {

    private static final long serialVersionUID = 113654781L;

    public static final QJcbBjJdb jcbBjJdb = new QJcbBjJdb("jcbBjJdb");

    public final StringPath bz = createString("bz");

    public final StringPath jddm = createString("jddm");

    public final StringPath jdmc = createString("jdmc");

    public final StringPath pxbh = createString("pxbh");

    public QJcbBjJdb(String variable) {
        super(JcbBjJdb.class, forVariable(variable));
    }

    public QJcbBjJdb(Path<? extends JcbBjJdb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjJdb(PathMetadata metadata) {
        super(JcbBjJdb.class, metadata);
    }

}

