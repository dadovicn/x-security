package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_AJTSB", schema = "FX110", catalog = "")
public class JcbBjAjtsb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "AJTSDM")
    private String ajtsdm;
    @Basic
    @Column(name = "AJDM")
    private String ajdm;
    @Basic
    @Column(name = "TSNR")
    private String tsnr;
    @Basic
    @Column(name = "BZ")
    private String bz;
}