package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data

@Table(name = "JCB_BJ_SFDZB", schema = "FX110", catalog = "")
public class JcbBjSfdzb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "SFDZDM")
    private String sfdzdm;
    @Basic
    @Column(name = "SFDZMC")
    private String sfdzmc;
    @Basic
    @Column(name = "SJSFDZ")
    private String sjsfdz;
    @Basic
    @Column(name = "BZ")
    private String bz;
}