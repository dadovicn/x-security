package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_BJXSB", schema = "FX110", catalog = "")
public class JcbBjBjxsb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "BJXSDM")
    private String bjxsdm;
    @Basic
    @Column(name = "BJXSMC")
    private String bjxsmc;
    @Basic
    @Column(name = "BZ")
    private String bz;
}