package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjXqb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjXqb is a Querydsl query type for JcbBjXqb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjXqb extends EntityPathBase<JcbBjXqb> {

    private static final long serialVersionUID = 113668638L;

    public static final QJcbBjXqb jcbBjXqb = new QJcbBjXqb("jcbBjXqb");

    public final StringPath bz = createString("bz");

    public final StringPath pxbh = createString("pxbh");

    public final StringPath sjxqdm = createString("sjxqdm");

    public final StringPath xqdm = createString("xqdm");

    public final StringPath xqmc = createString("xqmc");

    public QJcbBjXqb(String variable) {
        super(JcbBjXqb.class, forVariable(variable));
    }

    public QJcbBjXqb(Path<? extends JcbBjXqb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjXqb(PathMetadata metadata) {
        super(JcbBjXqb.class, metadata);
    }

}

