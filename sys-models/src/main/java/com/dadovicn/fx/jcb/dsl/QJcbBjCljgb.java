package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjCljgb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjCljgb is a Querydsl query type for JcbBjCljgb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjCljgb extends EntityPathBase<JcbBjCljgb> {

    private static final long serialVersionUID = 1841846801L;

    public static final QJcbBjCljgb jcbBjCljgb = new QJcbBjCljgb("jcbBjCljgb");

    public final StringPath bz = createString("bz");

    public final StringPath cljgdm = createString("cljgdm");

    public final StringPath cljgmc = createString("cljgmc");

    public final StringPath pxbh = createString("pxbh");

    public QJcbBjCljgb(String variable) {
        super(JcbBjCljgb.class, forVariable(variable));
    }

    public QJcbBjCljgb(Path<? extends JcbBjCljgb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjCljgb(PathMetadata metadata) {
        super(JcbBjCljgb.class, metadata);
    }

}

