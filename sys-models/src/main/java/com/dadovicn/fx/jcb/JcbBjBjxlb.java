package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

/**
 * 细类
 * @author dadovicn
 * @date   2018/10/3
 */
@Entity
@Data
@Table(name = "JCB_BJ_BJXLB", schema = "FX110", catalog = "")
public class JcbBjBjxlb {
    @Basic                
    @Column(name = "PXBH")
    private String pxbh;
    @Id                     
    @Column(name = "BJXLDM")
    private String bjxldm;
    @Basic                  
    @Column(name = "BJXLMC")
    private String bjxlmc;
    @Basic
    @Column(name = "BJLXDM")
    private String bjlxdm;
    @Basic                  
    @Column(name = "XLYSDM")
    private String xlysdm;
    @Basic                
    @Column(name = "TSGN")
    private String tsgn;
    @Basic              
    @Column(name = "BZ")
    private String bz;
}