package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

/**
 * 基础表1
 * @author dadovicn
 * @date   2018/9/9
 */
@Entity
@Data

@Table(name = "JCB_BJ_JQJDB", schema = "FX110", catalog = "")
public class JcbBjJqjdb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;

    @Id
    @Column(name = "JQJDDM")
    private String jqjddm;

    @Basic
    @Column(name = "JQJDMC")
    private String jqjdmc;

    @Basic
    @Column(name = "BZ")
    private String bz;
}