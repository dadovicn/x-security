package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_AJJBB", schema = "FX110", catalog = "")
public class JcbBjAjjbb {
    @Basic@Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "AJJBDM")
    private String ajjbdm;
    @Basic
    @Column(name = "AJJBMC")
    private String ajjbmc;
    @Basic
    @Column(name = "BZ")
    private String bz;
}