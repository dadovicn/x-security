package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjXqdwb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjXqdwb is a Querydsl query type for JcbBjXqdwb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjXqdwb extends EntityPathBase<JcbBjXqdwb> {

    private static final long serialVersionUID = 1861384427L;

    public static final QJcbBjXqdwb jcbBjXqdwb = new QJcbBjXqdwb("jcbBjXqdwb");

    public final StringPath dwmc = createString("dwmc");

    public final StringPath guid = createString("guid");

    public final StringPath nbdm = createString("nbdm");

    public final StringPath nbmc = createString("nbmc");

    public final StringPath xqdm = createString("xqdm");

    public final StringPath xqmc = createString("xqmc");

    public QJcbBjXqdwb(String variable) {
        super(JcbBjXqdwb.class, forVariable(variable));
    }

    public QJcbBjXqdwb(Path<? extends JcbBjXqdwb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjXqdwb(PathMetadata metadata) {
        super(JcbBjXqdwb.class, metadata);
    }

}

