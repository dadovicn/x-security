package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_DW_DWB", schema = "FX110", catalog = "")
public class JcbDwDwb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "DWDM")
    private String dwdm;
    @Basic
    @Column(name = "DWMC")
    private String dwmc;
    @Basic
    @Column(name = "NBDWDM")
    private String nbdwdm;
    @Basic
    @Column(name = "NBDWMC")
    private String nbdwmc;
    @Basic
    @Column(name = "SJDWDM")
    private String sjdwdm;
    @Basic
    @Column(name = "DHHM")
    private String dhhm;
    @Basic
    @Column(name = "TSGN")
    private String tsgn;
    @Basic
    @Column(name = "BZ")
    private String bz;
    @Basic
    @Column(name = "DATAIP")
    private String dataip;
    @Basic
    @Column(name = "DATAUSER")
    private String datauser;
    @Basic
    @Column(name = "DATAPWD")
    private String datapwd;
    @Basic
    @Column(name = "ZB")
    private String zb;
    @Basic
    @Column(name = "DWNJGX")
    private String dwnjgx;
    @Basic
    @Column(name = "TSGN_KZ")
    private String tsgnKz;
}