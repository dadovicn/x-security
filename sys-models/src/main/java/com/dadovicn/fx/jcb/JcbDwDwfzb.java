package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_DW_DWFZB", schema = "FX110", catalog = "")
public class JcbDwDwfzb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "DWFZDM")
    private String dwfzdm;
    @Basic
    @Column(name = "DWFZMC")
    private String dwfzmc;
    @Basic
    @Column(name = "BZ")
    private String bz;
    @Basic
    @Column(name = "NBDWFZDM")
    private String nbdwfzdm;
}