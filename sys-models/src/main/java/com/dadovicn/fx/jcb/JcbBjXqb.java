package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_XQB", schema = "FX110", catalog = "")
public class JcbBjXqb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "XQDM")
    private String xqdm;
    @Basic
    @Column(name = "XQMC")
    private String xqmc;
    @Basic
    @Column(name = "SJXQDM")
    private String sjxqdm;
    @Basic
    @Column(name = "BZ")
    private String bz;
}