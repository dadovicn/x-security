package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_LHLBB", schema = "FX110", catalog = "")
public class JcbBjLhlbb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;

    @Id
    @Column(name = "LHLBDM")
    private String lhlbdm;

    @Basic
    @Column(name = "LHLBMC")
    private String lhlbmc;

    @Basic
    @Column(name = "BZ")
    private String bz;
}