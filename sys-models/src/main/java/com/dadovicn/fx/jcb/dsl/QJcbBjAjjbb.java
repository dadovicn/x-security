package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjAjjbb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjAjjbb is a Querydsl query type for JcbBjAjjbb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjAjjbb extends EntityPathBase<JcbBjAjjbb> {

    private static final long serialVersionUID = 1839940022L;

    public static final QJcbBjAjjbb jcbBjAjjbb = new QJcbBjAjjbb("jcbBjAjjbb");

    public final StringPath ajjbdm = createString("ajjbdm");

    public final StringPath ajjbmc = createString("ajjbmc");

    public final StringPath bz = createString("bz");

    public final StringPath pxbh = createString("pxbh");

    public QJcbBjAjjbb(String variable) {
        super(JcbBjAjjbb.class, forVariable(variable));
    }

    public QJcbBjAjjbb(Path<? extends JcbBjAjjbb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjAjjbb(PathMetadata metadata) {
        super(JcbBjAjjbb.class, metadata);
    }

}

