package com.dadovicn.fx.jcb.dsl;

import com.dadovicn.fx.jcb.JcbBjLhlbb;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QJcbBjLhlbb is a Querydsl query type for JcbBjLhlbb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QJcbBjLhlbb extends EntityPathBase<JcbBjLhlbb> {

    private static final long serialVersionUID = 1850041093L;

    public static final QJcbBjLhlbb jcbBjLhlbb = new QJcbBjLhlbb("jcbBjLhlbb");

    public final StringPath bz = createString("bz");

    public final StringPath lhlbdm = createString("lhlbdm");

    public final StringPath lhlbmc = createString("lhlbmc");

    public final StringPath pxbh = createString("pxbh");

    public QJcbBjLhlbb(String variable) {
        super(JcbBjLhlbb.class, forVariable(variable));
    }

    public QJcbBjLhlbb(Path<? extends JcbBjLhlbb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QJcbBjLhlbb(PathMetadata metadata) {
        super(JcbBjLhlbb.class, metadata);
    }

}

