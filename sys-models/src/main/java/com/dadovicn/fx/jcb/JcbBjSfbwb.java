package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_SFBWB", schema = "FX110", catalog = "")
public class JcbBjSfbwb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "SFBWDM")
    private String sfbwdm;
    @Basic
    @Column(name = "SFBWMC")
    private String sfbwmc;
    @Basic
    @Column(name = "SJSFBW")
    private String sjsfbw;
    @Basic
    @Column(name = "BZ")
    private String bz;
}