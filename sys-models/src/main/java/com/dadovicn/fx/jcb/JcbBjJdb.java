package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_JDB", schema = "FX110", catalog = "")
public class JcbBjJdb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;

    @Id
    @Column(name = "JDDM")
    private String jddm;

    @Basic
    @Column(name = "JDMC")
    private String jdmc;

    @Basic
    @Column(name = "BZ")
    private String bz;
}