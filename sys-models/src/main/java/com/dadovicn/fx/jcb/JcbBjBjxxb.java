package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;
@Data
@Entity
@Table(name = "JCB_BJ_BJXXB", schema = "FX110", catalog = "")
public class JcbBjBjxxb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;
    @Id
    @Column(name = "BJXXDM")
    private String bjxxdm;
    @Basic
    @Column(name = "BJXXMC")
    private String bjxxmc;
    @Basic
    @Column(name = "XXYSDM")
    private String xxysdm;
    @Basic
    @Column(name = "TSGN")
    private String tsgn;
    @Basic
    @Column(name = "BZ")
    private String bz;
}