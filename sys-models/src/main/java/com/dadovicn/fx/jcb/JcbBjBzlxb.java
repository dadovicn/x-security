package com.dadovicn.fx.jcb;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "JCB_BJ_BZLXB", schema = "FX110", catalog = "")
public class JcbBjBzlxb {
    @Basic
    @Column(name = "PXBH")
    private String pxbh;

    @Id
    @Column(name = "BZLXDM")
    private String bzlxdm;

    @Basic
    @Column(name = "BZLXMC")
    private String bzlxmc;

    @Basic
    @Column(name = "BZ")
    private String bz;
}