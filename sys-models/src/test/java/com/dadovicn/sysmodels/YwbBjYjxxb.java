package com.dadovicn.sysmodels;

import javax.persistence.*;

@Entity
@Table(name = "YWB_BJ_YJXXB", schema = "FX110", catalog = "")
public class YwbBjYjxxb {
    private long id;
    private Long xsaj;
    private Long zaaj;
    private Long qzqz;
    private Long jtljq;
    private Long hzsg;
    private Long jbts;
    private Long shld;
    private Long qtxsj;
    private Long zhsg;
    private Long xzwf;
    private Long qtjq;
    private String type;

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "XSAJ")
    public Long getXsaj() {
        return xsaj;
    }

    public void setXsaj(Long xsaj) {
        this.xsaj = xsaj;
    }

    @Basic
    @Column(name = "ZAAJ")
    public Long getZaaj() {
        return zaaj;
    }

    public void setZaaj(Long zaaj) {
        this.zaaj = zaaj;
    }

    @Basic
    @Column(name = "QZQZ")
    public Long getQzqz() {
        return qzqz;
    }

    public void setQzqz(Long qzqz) {
        this.qzqz = qzqz;
    }

    @Basic
    @Column(name = "JTLJQ")
    public Long getJtljq() {
        return jtljq;
    }

    public void setJtljq(Long jtljq) {
        this.jtljq = jtljq;
    }

    @Basic
    @Column(name = "HZSG")
    public Long getHzsg() {
        return hzsg;
    }

    public void setHzsg(Long hzsg) {
        this.hzsg = hzsg;
    }

    @Basic
    @Column(name = "JBTS")
    public Long getJbts() {
        return jbts;
    }

    public void setJbts(Long jbts) {
        this.jbts = jbts;
    }

    @Basic
    @Column(name = "SHLD")
    public Long getShld() {
        return shld;
    }

    public void setShld(Long shld) {
        this.shld = shld;
    }

    @Basic
    @Column(name = "QTXSJ")
    public Long getQtxsj() {
        return qtxsj;
    }

    public void setQtxsj(Long qtxsj) {
        this.qtxsj = qtxsj;
    }

    @Basic
    @Column(name = "ZHSG")
    public Long getZhsg() {
        return zhsg;
    }

    public void setZhsg(Long zhsg) {
        this.zhsg = zhsg;
    }

    @Basic
    @Column(name = "XZWF")
    public Long getXzwf() {
        return xzwf;
    }

    public void setXzwf(Long xzwf) {
        this.xzwf = xzwf;
    }

    @Basic
    @Column(name = "QTJQ")
    public Long getQtjq() {
        return qtjq;
    }

    public void setQtjq(Long qtjq) {
        this.qtjq = qtjq;
    }

    @Basic
    @Column(name = "TYPE")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        YwbBjYjxxb that = (YwbBjYjxxb) o;

        if (id != that.id) return false;
        if (xsaj != null ? !xsaj.equals(that.xsaj) : that.xsaj != null) return false;
        if (zaaj != null ? !zaaj.equals(that.zaaj) : that.zaaj != null) return false;
        if (qzqz != null ? !qzqz.equals(that.qzqz) : that.qzqz != null) return false;
        if (jtljq != null ? !jtljq.equals(that.jtljq) : that.jtljq != null) return false;
        if (hzsg != null ? !hzsg.equals(that.hzsg) : that.hzsg != null) return false;
        if (jbts != null ? !jbts.equals(that.jbts) : that.jbts != null) return false;
        if (shld != null ? !shld.equals(that.shld) : that.shld != null) return false;
        if (qtxsj != null ? !qtxsj.equals(that.qtxsj) : that.qtxsj != null) return false;
        if (zhsg != null ? !zhsg.equals(that.zhsg) : that.zhsg != null) return false;
        if (xzwf != null ? !xzwf.equals(that.xzwf) : that.xzwf != null) return false;
        if (qtjq != null ? !qtjq.equals(that.qtjq) : that.qtjq != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (xsaj != null ? xsaj.hashCode() : 0);
        result = 31 * result + (zaaj != null ? zaaj.hashCode() : 0);
        result = 31 * result + (qzqz != null ? qzqz.hashCode() : 0);
        result = 31 * result + (jtljq != null ? jtljq.hashCode() : 0);
        result = 31 * result + (hzsg != null ? hzsg.hashCode() : 0);
        result = 31 * result + (jbts != null ? jbts.hashCode() : 0);
        result = 31 * result + (shld != null ? shld.hashCode() : 0);
        result = 31 * result + (qtxsj != null ? qtxsj.hashCode() : 0);
        result = 31 * result + (zhsg != null ? zhsg.hashCode() : 0);
        result = 31 * result + (xzwf != null ? xzwf.hashCode() : 0);
        result = 31 * result + (qtjq != null ? qtjq.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}