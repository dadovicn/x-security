package com.dadovicn.auth;

import com.dadovicn.auth.vo.SysUserVO;
import com.dadovicn.web.Result;

import java.util.List;

/**
 * create by 段董董 on 2018/9/3 - 下午11:44
 */
public interface UserService {
    List<SysUser> findBySearchTerm();

    /**
     * 保存用户
     * @param sysUserVO
     * @return
     */
    Result saveSysUser(SysUserVO sysUserVO);

    /**
     * geng
     * @param sysUserVO
     * @return
     */
    Result updateSysUser(SysUserVO sysUserVO);
}
