package com.dadovicn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 入口
 * @author dadovicn
 * @date   2018/8/29
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.dadovicn"})
@EnableScheduling
@EnableCaching
public class SysApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SysApiApplication.class, args);
	}
}
