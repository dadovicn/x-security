package com.dadovicn.sec;

import java.io.Serializable;

/**
 * 响应体
 * @author dadovicn
 * @date   2018/8/28
 */
public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;
    private final String status = "ok";

    public JwtAuthenticationResponse(String token) {
        this.token = token;
    }
    public String getStatus() { return this.status; }
    public String getToken() {
        return this.token;
    }
}
