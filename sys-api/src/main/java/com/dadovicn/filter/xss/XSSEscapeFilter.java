package com.dadovicn.filter.xss;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * for xss
 * @author dadovicn
 * @date   2018/9/14
// */
//public class XSSEscapeFilter implements Filter {
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {}
//
//    @Override
//    public void destroy() {}
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        chain.doFilter(new XssHttpServletRequestWrapper((HttpServletRequest) request), response);
//    }
//}