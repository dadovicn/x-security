package com.dadovicn.web.cron;

import com.dadovicn.repo.primary.fx.CacheManageService;
import com.dadovicn.repo.primary.fx.JcjRepo;
import com.dadovicn.repo.primary.fx.JjAnalysisRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @desc 定时任务类
 * @author dadovicn
 * @date   2018/11/18d
 */
@Component
public class CronController{

    @Autowired
    private CacheManageService cacheManageService;

    @Autowired
    private JjAnalysisRepo jjAnalysisRepo;

    private AtomicInteger step = new AtomicInteger(1);

    @Autowired
    JcjRepo jcjRepo;

    /**
     * 每天凌晨4点出报表/ 上缓存
     */
    @Scheduled(cron = "0 30 4 * * ?")
    public void cronReport() {
        if(step.get() == 1) {
            jjAnalysisRepo.getReportYearMonth();
            jjAnalysisRepo.getReportMonthDay();
            jcjRepo.getHeatMap();
            jcjRepo.getWarnMonth();
            jcjRepo.getWarnWeek();
            jcjRepo.getMonthRate();
            jcjRepo.getWeekRate();
        }else {
            cacheManageService.evictAllCaches();
            jjAnalysisRepo.getReportMonthDay();
            jjAnalysisRepo.getReportYearMonth();
            jcjRepo.getWarnMonth();
            jcjRepo.getWarnWeek();
            jcjRepo.getMonthRate();
            jcjRepo.getWeekRate();
            jcjRepo.getHeatMap();

        }
        step.incrementAndGet();
    }

}