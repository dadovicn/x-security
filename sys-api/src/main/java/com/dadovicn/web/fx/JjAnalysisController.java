package com.dadovicn.web.fx;

import com.dadovicn.repo.primary.fx.JjAnalysisRepo;
import com.dadovicn.web.Result;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 接警数据分析
 * @author dadovicn
 * @date   2018/10/4
 */
@RestController
@RequestMapping("/api/jjAnalysis")
public class JjAnalysisController {

    @Resource
    private JjAnalysisRepo jjAnalysisRepo;

    @RequestMapping(value = {"/getBjlbPie/{pieXqdm}", "/getBjlbPie/" })
    public Result getBjlbPie(@RequestBody List<LocalDateTime> date, @PathVariable(required = false) String pieXqdm) {
        return jjAnalysisRepo.getBjlbPie(date, pieXqdm);
    }

    @RequestMapping(value = {"/getBjlxPolarintervalByBjlbmc/{bjlbmc}/{pieXqdm}", "/getBjlxPolarintervalByBjlbmc/{bjlbmc}/"} )
    public Result getBjlxPolarintervalByBjlbmc(@RequestBody List<LocalDateTime> date, @PathVariable String bjlbmc, @PathVariable(required = false) String pieXqdm) {
        return jjAnalysisRepo.getBjlxPolarintervalByBjlbmc(bjlbmc, date, pieXqdm);
    }

    @RequestMapping(value = {"/getBjxlPyramidByBjlxmc/{bjlxmc}/{pieXqdm}", "/getBjxlPyramidByBjlxmc/{bjlxmc}/"})
    public Result getBjxlPyramidByBjlxmc(@RequestBody List<LocalDateTime> date, @PathVariable String bjlxmc, @PathVariable(required = false) String pieXqdm) {
        return jjAnalysisRepo.getBjxlPyramidByBjlxmc(bjlxmc, date, pieXqdm);
    }
}