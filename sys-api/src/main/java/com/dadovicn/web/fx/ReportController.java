package com.dadovicn.web.fx;

import com.dadovicn.fx.ywb.vo.ReportVO;
import com.dadovicn.repo.primary.fx.JjAnalysisRepo;
import com.dadovicn.web.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 生成报表
 * @author dadovicn
 * @date   2018/10/3
 */
@RestController
@RequestMapping("/api/report")
public class ReportController {
    @Resource
    private JjAnalysisRepo jjAnalysisRepo;

    /**
     * 统计当年每月的接警数据
     * @return
     */
    @PostMapping("/getReport")
    public Result getReport() {
        return  this.jjAnalysisRepo.getReportYearMonth();
    }

    /**
     * 统计当月每天的接警数据
     * @return
     */
    @PostMapping("/getReportMonthDay")
    public Result getReportMonthDay() {
        return this.jjAnalysisRepo.getReportMonthDay();
    }

}