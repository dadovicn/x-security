package com.dadovicn.web.fx;

import com.dadovicn.fx.ywb.qo.CalendarQO;
import com.dadovicn.fx.ywb.qo.CountQO;
import com.dadovicn.fx.ywb.qo.JcjQO;
import com.dadovicn.repo.primary.fx.JcjRepo;
import com.dadovicn.web.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * 接处警数据基本查询
 * @author dadovicn
 * @date   2018/9/13
 */
@RestController
@RequestMapping("/api/jcj")
public class JcjController {

    @Resource
    private JcjRepo jcjRepo;

    @PostMapping("/findJcjByJjbh/{jjbh}")
    public Result findJcjByJjbh(@PathVariable String jjbh) {
        return jcjRepo.findJcjByJjbh(jjbh);
    }

    @PostMapping("/findCjjByJjbh/{jjbh}")
    public Result findCjjByJjbh(@PathVariable String jjbh) {
        return jcjRepo.findCjjByJjbh(jjbh);
    }

    /**
     * 接处警列表
     * @param jcjQO 查询对象
     * @param currentPage 当前页码
     * @param pageSize 页容量
     * @return result
     */
    @PostMapping("/listJcj/{currentPage}/{pageSize}")
    public Result listJcj(@RequestBody JcjQO jcjQO, @PathVariable String currentPage, @PathVariable String pageSize) {
        return jcjRepo.listJcj(jcjQO, Integer.valueOf(currentPage) - 1, Integer.valueOf(pageSize));
    }

    /**
     * excel 导出
     * @param jcjQO 查询对象
     * @param response 响应
     */
    @RequestMapping(value = "/exportExcel", method = RequestMethod.GET)
    public void exportXls(JcjQO jcjQO, HttpServletResponse response) {
        jcjRepo.jcjExport(jcjQO, response);
    }

    /**
     * 当日接警总量 & 日环比
     * @return result
     */
    @PostMapping(value = "/countYwbJqJjjlbToday")
    public Result countYwbJqJjjlbToday() {
        return jcjRepo.countYwbJqJjjlbToday();
    }

    /**
     * 当日出警总量 & 日环比
     * @return result
     */
    @PostMapping(value = "/countYwbJqCjjlbToday")
    public Result countYwbJqCjjlbToday() {
        return jcjRepo.countYwbJqCjjlbToday();
    }

    /**
     * 刑事案件总量 & 日环比
     * @return
     */
    @RequestMapping("/countXsajToday")
    public Result countXsajToday() {
        return jcjRepo.countXsajToday();
    }

    @RequestMapping(value = "/countYwbJqJjjlbValid")
    public Result countYwbJqJjjlbValid() {
        Result result = jcjRepo.countYwbJqJjjlbValid();
        return result;
    }

    /**
     * 高频词云
     * @return result
     */
    @PostMapping(value = "/listHighFreq")
    public Result listHighFreq(@RequestBody CountQO qo) {
        return jcjRepo.listHighFreq(qo);
    }

    /**
     * 接警数据统计: 案件类型/时间
     * @return result
     */
    @PostMapping(value = "/primaryMonitor")
    public Result primaryMonitor(@RequestBody CountQO qo) {
        return jcjRepo.primaryMonitor(qo);
    }

    /**
     * 出警数据统计: 案件类型/时间
     * @return
     */
    @PostMapping(value = "/primaryMonitorForCj")
    public Result primaryMonitorForCj(@RequestBody CountQO qo) {
        System.out.println(qo);
        return jcjRepo.primaryMonitorForCj(qo);
    }

    /**
     * 辖区案发排名d
     * @return
     */
    @PostMapping(value = "/xqRanking")
    public Result xqRanking(@RequestBody CountQO qo) {
        return jcjRepo.xqRanking(qo);
    }

    /**
     * 辖区案发排名
     * @return
     */
    @PostMapping(value = "/xqRankingCj")
    public Result xqRankingCj(@RequestBody CountQO qo) {
        return jcjRepo.xqRankingCj(qo);
    }

    /**
     * 获取近一年水平日历热力图
     * @return
     */
    @PostMapping(value = "getCalendarhorizontalData")
    public Result getCalendarhorizontalData(@RequestBody CalendarQO calendarQO) {
        Result result = jcjRepo.getCalendarhorizontalData(calendarQO);
        return result;
    }

    /**
     * 预警图
     * @return
     */
    @PostMapping("/getHeatMap")
    public Result getHeatMap() {
        Result result = jcjRepo.getHeatMap();
        return result;
    }

    /**
     * 获取近一个月的数据
     * @return
     */
    @PostMapping(value = "/getRecentMonthJj")
    public Result getRecentMonthJj() {
        Result result = jcjRepo.getRecentMonthJj();
        return result;
    }

    /**
     * 預警信息
     * @return
     */
    @PostMapping(value = "/getDayRate")
    public Result getDayRate() {
        Result result = jcjRepo.getDayRate();
        return result;
    }

    /**
     * 預警信息
     * @return
     */
    @PostMapping(value = "/getWeekRate")
    public Result getWeekRate() {
        Result result = jcjRepo.getWeekRate();
        return result;
    }

    /**
     * 預警信息
     * @return
     */
    @PostMapping(value = "/getMonthRate")
    public Result getMonthRate() {
        Result result = jcjRepo.getMonthRate();
        return result;
    }

    @PostMapping(value = "/getWarnMonth")
    public Result getWarnMonth() {
        Result result = jcjRepo.getWarnMonth();
        return result;
    }

    @PostMapping(value = "/getWarnDay")
    public Result getWarnDay() {
        Result result = jcjRepo.getWarnDay();
        return result;
    }

    @PostMapping(value = "/getWarnWeek")
    public Result getWarnWeek() {
        Result result = jcjRepo.getWarnDay();
        return result;
    }


}