package com.dadovicn.web.fx;

import com.dadovicn.enums.SysDictEnum;
import com.dadovicn.fx.ywb.YwbBjYjxxb;
import com.dadovicn.fx.ywb.dto.TreeSelectDto;
import com.dadovicn.fx.ywb.qo.BjlbQO;
import com.dadovicn.fx.ywb.qo.DictQO;
import com.dadovicn.fx.ywb.vo.*;
import com.dadovicn.repo.primary.fx.CommonRepo;
import com.dadovicn.web.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 公共 api
 * @author dadovicn
 * @date   2018/10/2
 */
@RestController
@RequestMapping("/api/common")
public class CommonController {

    @Resource
    private CommonRepo commonRepo;

    @PostMapping(value = "/findDictByParentKey/{currentPage}/{pageSize}")
    public Result findDictByParentId(@RequestBody DictQO dictQO, @PathVariable int currentPage, @PathVariable int pageSize) {
        return commonRepo.findDictByParentId(dictQO.getParentId(), currentPage - 1, pageSize);
    }

    /**
     * 报警类别名称列表
     * @return result
     */
    @PostMapping("/findAllBjlb")
    public Result findAllBjlb() {
        List<String> bjlbList = commonRepo.findAllBjlb();
        return Result.success(bjlbList);
    }


    /**
     * 获取所有报警类别和代码| 报警类别表
     * @return result
     */
    @PostMapping("/listAllBjlb")
    public Result listAllBjlb() {
        List<TreeSelectDto> result = commonRepo.listAllBjlb();
        return Result.success(result);
    }

    /**
     * 获取所有辖区代码和名称
     * @return
     */
    @PostMapping("/listAllXq")
    public Result listAllXq() {
        List<XqVO> result = commonRepo.listAllXq();
        return Result.success(result);
    }

    /**
     * 查找所有报警类型 | 报警类型表
     * @param qo 查询对象
     * @return
     */
    @PostMapping("/findLxByLbdm")
    public Result findLxByLbdm(@RequestBody BjlbQO qo) {
        List<TreeSelectDto> result = commonRepo.findLxByLbdm(qo.getLbdm());
        return Result.success(result);
    }

    /**
     * 查找所有报警细类 | 报警细类表
     * @param qo 报警类型代码
     * @return
     */
    @PostMapping("/findXlByLxdm")
    public Result findXlByLxdm(@RequestBody BjlbQO qo) {
        List<TreeSelectDto> result = commonRepo.findXlByLxdm(qo.getLxdm());
        return Result.success(result);
    }

    /**
     * 预警信息
     * @return
     */
    @PostMapping("/listYjxx")
    public Result listYjxx() {
        return commonRepo.listYjxx();
    }

    @PostMapping("/getYjxx")
    public Result getYjxx() {
        return commonRepo.getYjxx();
    }

    @PostMapping("/updateYjxx")
    public Result updateYjxx(@RequestBody YwbBjYjxxb ywbBjYjxxb) {
        return commonRepo.updateYjxx(ywbBjYjxxb);
    }
}