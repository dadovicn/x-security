package com.dadovicn.web.auth;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 角色测试入口
 * @author dadovicn
 * @date   2018/8/29
 */
@RestController
@RequestMapping("protected")
public class MethodProtectedRestController {

    @RequestMapping(method = RequestMethod.GET, value = "/governor")
    @PreAuthorize("hasRole('GOVERNOR')")
    public ResponseEntity<?> getProtectedGreeting() {
        System.out.println("省长权限");
        return ResponseEntity.ok("省长权限");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/mayor")
    @PreAuthorize("hasRole('MAYOR')")
    public ResponseEntity<?> getMajor() {
        System.out.println("市长权限");
        return ResponseEntity.ok("市长权限");
    }
}