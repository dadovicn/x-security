package com.dadovicn.web.auth;

/**
 * 异常
 * @author dadovicn
 * @date   2018/8/29
 */
public class AuthenticationException extends RuntimeException {
    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }
}