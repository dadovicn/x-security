package com.dadovicn.web.user;

import com.dadovicn.auth.SysUser;
import com.dadovicn.auth.SysUserQO;
import com.dadovicn.auth.UserService;
import com.dadovicn.auth.vo.SysUserVO;
import com.dadovicn.repo.primary.auth.SysUserRepo;
import com.dadovicn.repo.primary.auth.impl.SysUserRepoImpl;
import com.dadovicn.web.Result;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <h3>用户信息类</h3>
 * <p>1. 拒绝使用swagger, 因为老子前后端通吃.</p>
 * <p>2. 拒绝写注释, 好的方法名, 清晰的代码逻辑一般不需要注释</p>
 * @author dadovicn
 * @date   2018/8/30
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Resource
    private SysUserRepo sysUserRepo;
    @Resource
    private UserService userService;

    @GetMapping("/getCurrentUser/{username}")
    public Result getCurrentUser(@PathVariable String username) {
        SysUser sysUser = sysUserRepo.findSysUserByUserName(username);
        return Result.success(sysUser);
    }

    @PostMapping("/listUser/{currentPage}/{pageSize}")
    public Result listUser(@RequestBody(required = false) SysUserQO sysUserQO, @PathVariable int currentPage, @PathVariable int pageSize) {
        Pageable pageable = PageRequest.of(currentPage - 1, pageSize);
        System.out.println(sysUserQO);
        Page<SysUser> pageList = sysUserRepo.findAll(pageable);
        List<SysUser> sysUserList = pageList.getContent();
        return Result.success(sysUserList, pageList.getTotalElements());
    }

    @PostMapping("/saveUser")
    public Result saveUser(@RequestBody SysUserVO sysUserVO) {
        return userService.saveSysUser(sysUserVO);
    }

    @PostMapping("/deleteUser/{userId}")
    public Result deleteUser(@PathVariable Long userId) {
        sysUserRepo.deleteById(userId);
        return Result.success();
    }

    @PostMapping("/updateUser")
    public Result updateUser(@RequestBody SysUserVO sysUserVO) {
        return userService.updateSysUser(sysUserVO);
    }
}