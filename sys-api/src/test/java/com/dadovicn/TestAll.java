package com.dadovicn;

import com.dadovicn.auth.SysUser;
import com.dadovicn.auth.SysUserLocalAuth;
import com.dadovicn.repo.primary.auth.SysUserRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;

/**
 * create by 段董董 on 2018/8/26 - 上午7:43
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAll {
    @Resource
    SysUserRepo sysUserRepo;

    @Test
    @Transactional
    public void testRepo() {
        SysUser sysUser = sysUserRepo.findSysUserByUserName("user_2");
        System.out.println("fuck");

    }

    /**
     * 生成测试密码
     */
    @Test
    public void testBCryptPasswordEncoder() {
        CharSequence rawPassword = "dadovicn";
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodePasswd = encoder.encode(rawPassword);
        boolean isMatch = encoder.matches(rawPassword, encodePasswd);
        System.out.println("encodePasswd:" + encodePasswd);
        System.out.println(isMatch);
    }

    /**
     * 生成测试 用户和密码和角色
     */
    @Test
    public void testInsert() {
        for(int i = 10; i <=19; i++) {
            SysUser sysUser = new SysUser();
            sysUser.setUserName("test_" + i);
            sysUser.setMobile("130277831" + i);
            sysUser.setEmail("test" + i + "@163.com");
            sysUser.setRealName("test_" + i);
            sysUser.setGroupId(4L);
            sysUser.setCreateTime(new Timestamp(System.currentTimeMillis()));
            sysUser.setUpdateTime(new Timestamp(System.currentTimeMillis()));
            sysUser.setStatus(true);
            SysUserLocalAuth sysUserLocalAuth = new SysUserLocalAuth();
            sysUserLocalAuth.setPassword("$2a$10$dvHVaTrHrtWlhbeSDYrAY.mIAR83CfWNz0u6eM4PMUvHodH1gGhqu");
            sysUserLocalAuth.setSalt("salt");
            sysUserLocalAuth.setCreateTime(new Timestamp(System.currentTimeMillis()));
            sysUserLocalAuth.setUpdateTime(new Timestamp(System.currentTimeMillis()));
            sysUserLocalAuth.setStatus(true);
            sysUser.setSysUserLocalAuth(sysUserLocalAuth);
            sysUserRepo.save(sysUser);
        };

    }

}