package com.dadovicn.repo.primary.fx;

import com.dadovicn.repo.primary.fx.imp.JcjRepoImp;
import com.dadovicn.web.Result;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 接警表业务测试
 * @author dadovicn
 * @date   2018/9/12
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class jcjRepoTest {

    @Autowired
    private JcjRepoImp jcjRepoImp;

    // service-test-00001 [测试基于dsl的基础查询]
    /**
     * [1] 时间日期封装使用 localDateTime
     * [2] Projections.constructor() 可做dto映射
     * [3]
     */
    @Test
    public void testListJcj() {
//        JcjQO ywbJqJjjlb = new JcjQO();
//        ywbJqJjjlb.setBjsjks(LocalDateTime.now().minusDays(10L));
//        List<YwbJjjlbFissionDto> jj =  jcjRepoImpl.listJcj(ywbJqJjjlb);
//        jj.forEach(i -> {
//            System.out.println(i.toString());
//        });
//        System.out.println("dd");
    }

    // service-test-00002 [获取当日数据]
    /**
     * [1]
     * [2]
     * [3]
     */
    @Test
    public void testCountYwbJqJjjlbToday() {
        Result oo = jcjRepoImp.countYwbJqJjjlbToday();
        System.out.println(8);
    }

    // service-test-00003 [测试高频词云]
    /**
     * [1]
     * [2]
     * [3]
     */
    @Test
    public void testlistHighFreq() {
//        jcjRepoImp.listHighFreq();
    }

    // service-test-00000 [测试日历热力图]
    /**
     * [1]
     * [2]
     * [3]
     */
    @Test
    public void testGetCalendarhorizontalData() {
//        Result oo = jcjRepoImp.getCalendarhorizontalData();
    }

    @Test
    public void testWarn() {
        Result jj = jcjRepoImp.getWeekRate();
        Result dd = jcjRepoImp.getMonthRate();
        System.out.println("asdf");
    }

    @Test
    public void testWarn2() {
        Result jj  = jcjRepoImp.getHeatMap();
        System.out.println("asdf");
    }
}