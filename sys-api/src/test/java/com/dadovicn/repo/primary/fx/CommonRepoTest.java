package com.dadovicn.repo.primary.fx;


import com.dadovicn.fx.ywb.dto.TreeSelectDto;
import com.dadovicn.fx.ywb.vo.*;
import com.dadovicn.web.Result;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommonRepoTest {

    @Resource
    private CommonRepo commonRepo;

    @Test
    public void testFindDictByParentId() {
//        Result result = commonRepo.findDictByParentId(1L);
        System.out.println("abc");
    }

    @Test
    public void testListAllBjlb() {
//        List<BjlbVO> result = commonRepo.listAllBjlb();
    }

    @Test
    public void testListAllXq() {
        List<XqVO> result = commonRepo.listAllXq();
    }

    @Test
    public void testFindLxByLbdm() {
        List<TreeSelectDto> result = commonRepo.findLxByLbdm("05000000");
    }

    @Test
    public void testFindXlByLxdm() {
        List<TreeSelectDto> result = commonRepo.findXlByLxdm("01010000");
    }
}