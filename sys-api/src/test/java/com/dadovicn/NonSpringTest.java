package com.dadovicn;

import com.dadovicn.constant.Constant;
import com.dadovicn.enums.DayOfWeekEnum;
import com.dadovicn.enums.MonthOfYearEnum;
import com.dadovicn.enums.ReportEnum;
import com.dadovicn.fx.ywb.dsl.QYwbJqCjjlb;
import com.dadovicn.fx.ywb.qo.JcjQO;
import com.dadovicn.fx.ywb.vo.ClassAndCountVO;
import com.dadovicn.fx.ywb.vo.Coordinate;
import com.dadovicn.util.DateUtil;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.junit.Test;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 基础测试类
 * @author dadovicn
 * @date   2018/10/2
 */
public class NonSpringTest {

    // nonspring-test-00001 [测试list]
    /**
     * [1] 测试list即可求差集: pass
     * [2] 测试对list翻转操作是破坏性吗: pass
     * [3] 测试空对象: 获取集合中的空对象不会报错, 拿空对象做操作会报错
     */
    @Test
    public void testList() {
        /** 测试list即可求差集 */
        List<String> listA = new ArrayList<>();
        listA.add("同意");
        listA.add("不同意");
        listA.add("请派发");
        listA.add("已阅");
        listA.add("请优先处理");
        listA.add("请审阅");
        List<String> listB = new ArrayList<>();
        listB.add("同意");
        listB.add("不同意");
        listB.add("zhen");
        listB.removeAll(listA);
        System.out.println("结束");

        /** 测试对list翻转操作是破坏性吗 */
        List<Coordinate> test =  new ArrayList<>();
        test.add(new Coordinate("1", 2L));
        test.add(new Coordinate("2", 2L));
        test.add(new Coordinate("3", 2L));
        test.add(new Coordinate("4", 2L));
        System.out.println(test);
        Collections.reverse(test);
        System.out.println(test);

        /** 测试空对象 */
        Dad dd = new Dad();
        dd.setAge("kkk");
        List aa = new ArrayList();
        aa.add(null);
        aa.get(0);
        System.out.println(dd.getName());
    }
    
    // nonspring-test-00002 [字符串操作]
    /**
     * [1] 测试字符串是否包含小数: pass
     * [2] 基本数据类型参数传递: 不会改变
     * [3]
     */
    @Test
    public void testStringOper()  {
        System.out.println(StringUtils.isNumeric("-1"));
        Integer aa = 8888;
        forTestStringOper(aa);
        System.out.println(aa);
    }

    // non-spring-test-00003 [测试LocalDateTime]
    /**
     * [1] 完美映射 Oracle date 类型, 需要页面做展示好像不需要做转换 ✅
     * [2] 格式化时间标准 ✅
     * [3] 当天凌晨时间获取 ✅
     * [4] 一周中的第几天 ✅
     * [5] 字符传格式化为 localdate ✅
     * [6] 获取月份的最后七天 ✅
     * [7] 测试获取一年十二个月份
     */
    @Test
    public void testLocalDateTime() {
        System.out.println("下面是当前系统标准时间: -----------------------------------------------------");
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constant.DATE_TODAY)));
        System.out.println(LocalDateTime.now().compareTo(LocalDateTime.of(LocalDate.now(), LocalTime.MIN)));
        System.out.println(LocalDateTime.now().minusDays(1));
        System.out.println(LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.MIN));
        System.out.println(LocalTime.NOON);
        System.out.println(LocalDateTime.now().minusDays(0));
        System.out.println("上面是当前系统标准时间: -----------------------------------------------------");
        /** 一周中的第几天 */
        LocalDate now = LocalDate.now();
        System.out.println(now.getDayOfWeek());
        System.out.println(now.minusDays(1).getDayOfWeek());
        System.out.println(now.minusDays(2).getDayOfWeek());
        System.out.println(now.minusDays(3).getDayOfWeek());
        System.out.println(now.minusDays(4).getDayOfWeek());
        System.out.println(now.minusDays(5).getDayOfWeek());
        System.out.println(now.minusDays(6).getDayOfWeek());
        System.out.println(now.minusMonths(1).getMonth());
        System.out.println(now.minusMonths(2).getMonth());
        System.out.println(now.minusMonths(3).getMonth());
        System.out.println(now.minusMonths(4).getMonth());
        System.out.println(now.minusMonths(5).getMonth());
        System.out.println(now.minusMonths(6).getMonth());
        System.out.println(now.minusMonths(7).getMonth());
        System.out.println(now.minusMonths(8).getMonth());
        System.out.println(now.minusMonths(9).getMonth());
        System.out.println(now.minusMonths(10).getMonth());
        System.out.println(now.minusMonths(11).getMonth());
        System.out.println(now.minusMonths(12).getMonth());
        /** 字符传格式化为 localdate */
        LocalDate a = LocalDate.parse("2018-07-30", DateTimeFormatter.ISO_DATE);
        /** 获取月份最后7天 */
        LocalDate localDate = LocalDate.now();
        int step = 1;
        while (step < 100) {
            if(localDate.getDayOfMonth() > localDate.lengthOfMonth() -7) {
                System.out.println(localDate.toString());
            }
            localDate = localDate.plusDays(1L);
            step += 1;
        }

    }

    // nonspring-test-00004 [测试反射]
    /**
     * [1] 测试反射: pass
     * [2] 反射获取简单类名: pass
     * [3]
     */
    @Test
    public void testReflect() throws IllegalAccessException, InstantiationException, NoSuchFieldException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        JcjQO ywbJqJjjlbQO = new JcjQO();
        ywbJqJjjlbQO.setJjrxm("祝庆菊");
        Field field = ywbJqJjjlbQO.getClass().getDeclaredField("jjbh");
//        String[] jj = field.getAnnotation(ForClass.class).classPairs();
//        Constructor constructor = Class.forName(jj[0]).getDeclaredConstructor(String.class);
//        Class<?> cl = (Class<?>) Class.forName(jj[0]);
//        StringPath stringPath = (StringPath)Class.forName(jj[0]).getDeclaredField(field.getName()).get(constructor.newInstance("ywbJqCjjlb"));
//        System.out.println(stringPath);
        String zifu = QYwbJqCjjlb.class.getSimpleName().substring(1);
        String tmp = (new StringBuilder()).append(Character.toLowerCase(zifu.charAt(0))).append(zifu.substring(1)).toString();
        System.out.println(tmp);
    }

    // nonspring-test-00005 [测试正则表达式]
    /**
     * [1] 01 02 03 04 05 06 07 99 正则匹配: pass
     * [2]
     * [3]
     */
    @Test
    public void testRegex() {
        String regexA = "(^[0][^809]$)|99";
        String regex = "([0][^809])|99";
        String regexB = "01|02|03|04|05|06|07|99";
        Pattern pattern = Pattern.compile(regex);
        String strt[] = new String[] {"01","02","03","04","05","06","07","99","09"};
        Arrays.stream(strt).forEach(i ->  {
            System.out.println(i);
            Matcher m = pattern.matcher(i);
            System.out.println(m.matches());
        });
    }

    // nonspring-test-00006 [测试map函数]
    /**
     * [1] map 函数可以进行转换操作
     * [2]
     * [3]
     */
    @Test
    public void testMap() {
        List<ClassAndCountVO> classAndCountVOS= new ArrayList<>();
        ClassAndCountVO classAndCountVO = new ClassAndCountVO();
        classAndCountVO.setName("a");
        classAndCountVO.setValue(2L);
        classAndCountVOS.add( classAndCountVO);
        List<ClassAndCountVO> jj =  classAndCountVOS.stream().map(n ->
            new ClassAndCountVO( n.getValue() * 10, n.getName())
        ).collect(Collectors.toList());
        System.out.println(classAndCountVOS);
        System.out.println("fuck");
    }


    // nonspring-test-00007 [测试枚举]
    /**
     * [1] 可以通过 enum map 方式获取
     * [2] 可以通过 Enum.valueOf 方式获取
     * [3] 可以将这两种方法封装到工具类
     * [4] 枚举遍历
     */
    @Test
    public void testEnum() throws NoSuchFieldException {
        System.out.println(DayOfWeekEnum.FRIDAY);
        System.out.println(DayOfWeekEnum.FRIDAY);
        EnumMap<DayOfWeekEnum, Integer> day = new EnumMap<DayOfWeekEnum, Integer>(DayOfWeekEnum.class);
        day.put(DayOfWeekEnum.SUNDAY, 0);
        day.put(DayOfWeekEnum.MONDAY, 1);
        day.put(DayOfWeekEnum.TUESDAY, 2);
        day.put(DayOfWeekEnum.WEDNESDAY, 3);
        day.put(DayOfWeekEnum.THURSDAY, 4);
        day.put(DayOfWeekEnum.FRIDAY, 5);

        System.out.println(day.get(Enum.valueOf(DayOfWeekEnum.class, "FRIDAY")));
        System.out.println(Enum.valueOf(DayOfWeekEnum.class, "SUNDAY").getValue());

        Arrays.stream(MonthOfYearEnum.values()).forEach(i -> {
            System.out.println(i);
        });
    }

    // 辅助方法
    public void forTestStringOper(Integer lkj) {
        lkj = 1234;
    }

    @Test
    public void tmpTest() {
        // System.out.println(LocalDate.now() + " -"+ LocalDateTime.now().format(DateTimeFormatter.ofPattern(Constant.DATE_TODAY)));
        Date mm =  new Date();
//        mm.now
        System.out.println(mm.toString());
        System.out.println(mm.getTime());
        System.out.println(mm.getTime() + 604800 * 1000);
        System.out.println(new Date(mm.getTime() + 604800 * 1000));

    }

}

@Data
class Dad {
    private String age;
    private String name;
}