1，写一个ETL，同步110上面那个几个表的数据，完全照搬到一个新库。
2，写一个定时任务统计，可以很蠢，但是一定要准，根据我给你的报表exl，把案件类型、案件分类、时间、单位、值班员信息等要素串联起来，统计到新库。
3，web应用必须包含几个功能，基本查询，各种基本group统计和图表展现，预警设置，预警，报表导出，文档导出。

110 有几个比较重要的数据表    案件表、单位表、值班员信息表、接警表、处警表、反馈表、呼叫记录表

1. 单位表 jcb_dw_dwb
2. 案件级别表 
3. 案件表


    首页
       |
    ---- dashboard (这个就根据报表和库表进行拓展)
       |
       |____ 统计1 报表
       |
       |____ 统计2 报表...
       |
       |____ 统计2 图表...各种
          
    ---- 基本查询
       |
       |____ 接警情查询 [ 案件类型|警情摘要|接警员|报警电话|案发地址|处警单位|时间|报警人姓名| 警情摘要.....等等 | 时间| 导出| ] 
       |              |                                                                                                          
       |              |___ 接处警详情页面
       |
       |____ 呼叫记录 _>_ 详情页面
       |
    ---- 预警页面 
       |
    ---- 系统配置
       |
       |____ 字典项(维护一些定时任务啥的

       